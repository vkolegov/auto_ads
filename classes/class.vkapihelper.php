<?php

require_once BASEPATH . "/basic_functions.php";
require_once BASEPATH . "/classes/class.vkapi.php";
require_once BASEPATH . "/classes/class.vkapiscriptexecutor.php";

class VKApiHelper
{

    private $_api; // Объект класса VKAPI
    private $_scriptExecutor; // Объект класса VKAPIScriptExecutor
    private $_user_id;
    private $_groupId; // ID целевой группы
    private $_account_id; // ID рекламного кабинета
    private $_agency = false; // Является ли рекламный кабинет агенстким
    private $_albumId; // Альбом, в котором ведем подсчет активности
    private $_topicId; // Тема обсуждения, в которой ведем подсчет активности

    private $_logger = null;

    public function __construct()
    {
        $this->_logger = new Logger('VKApiHelper');
        $this->_logger->logInfo("[VKApiHelper][__construct()]");
        $this->_logger->logInfo("[VKApiHelper][__construct()] Вызываем VKApiHelper::readVKSettings()");
        $vk_settings = $this->readVKSettings();

        $this->_logger->logInfo("[VKApiHelper][__construct()] Создаем объект класса VKAPI");
        $this->_api = new VKAPI($vk_settings['access_token']);

        $this->_logger->logInfo("[VKApiHelper][__construct()] Создаем объект класса VKAPIScriptExecutor");
        $this->_scriptExecutor = new VKAPIScriptExecutor($this->_api);

        $this->_user_id = $vk_settings['user_id'];
        $this->_groupId = $vk_settings['group_id'];
        // $this->_albumId = $vk_settings['album_id'];
        // $this->_topicId = $vk_settings['topic_id'];
        $this->_account_id = $vk_settings['account_id'];
        $this->_agency = !empty($vk_settings['agency']);
        
        $this->_logger->logInfo("[VKApiHelper][__construct()] Объекта класса VKApiHelper создан");
    }

    /**
     * Считывает токен, id сообщества, id альбома и id обсуждения из файла настроек config
     *
     * @return array Массив настроек
     */
    private function readVKSettings()
    {
        $this->_logger->logInfo("[VKApiHelper][readVKSettings()]");

        $this->_logger->logInfo("[VKApiHelper][readVKSettings()] Вызываем read_settings()");

        $vk_settings = read_settings('vkapi');

        $this->_logger->logInfo("[VKApiHelper][readVKSettings()] Считанные настройки VK: {1}", $vk_settings);

        return $vk_settings;
    }

    /**
     * Возвращает ID сообщества
     *
     * @return string ID сообщества
     */
    public function getGroupId()
    {
        return $this->_groupId;
    }

    /**
     * Возвращает ID обсуждения
     *
     * @return string ID обсуждения
     */
    public function getTopicId()
    {
        return $this->_topicId;
    }

    /**
     * Является ли рекламный кабинет агенстким
     *
     * @return boolean
     */
    public function isAgency()
    {
        return $this->_agency;
    }

    // TODO: убрать. возможно переместить функционал в api_call
    private function getVKJSON($method_name, $params)
    {
        $this->_logger->logInfo("[VKApiHelper][getVKJSON()]");

        $this->_logger->logInfoI("[VKApiHelper][getVKJSON()] Method name: {1}", $method_name);
        $this->_logger->logInfoI("[VKApiHelper][getVKJSON()] Params: {1}", $params);

        $this->_logger->logInfo("[VKApiHelper][getVKJSON()] Вызываем VKAPI::api_call()");

        $response_json = $this->_api->api_call($method_name, $params);

        $this->_logger->logInfo("[VKApiHelper][getVKJSON()] Входим в цикл");
        while (true) {
            $counter = 0;
            // print "<pre>";
            // print_r($response_json);
            // print "</pre>";
            $this->_logger->logInfo("[VKApiHelper][getVKJSON()] Итерация цикла");
            if (isset($response_json['error'])) {
                $this->_logger->logError("[VKApiHelper][getVKJSON()] Ошибка VKAPI: {1}", $response_json);
                // Если ошибка - слишком много запросов в секунду
                $error_code = $response_json['error']['error_code'];
                if ($error_code == 6 || $error_code == 9) {
                    $this->_logger->logError("[VKApiHelper][getVKJSON()] Ошибка 6 или 9 - ждем");
                    // Ждем
                    usleep(2000000);
                    // Повторяем запрос
                    $this->_logger->logInfo("[VKApiHelper][getVKJSON()] Вызываем VKAPI::api_call()");
                    $response_json = $this->_api->api_call($method_name, $params);
                } else {
                    // Иначе - прекращаем выполнение и выводим ошибку
                    // print_r($response_json['error']);
                    break;
                    // TODO: Сделать обработку различных ошибок запроса
                }

            } else {
                $this->_logger->logInfo("[VKApiHelper][getVKJSON()] Ошибки не возникло - выходим из цикла и возвращаем ответ");
                return $response_json;
            }
        }
        $this->_logger->logInfo("[VKApiHelper][getVKJSON()] Вышли из цикла, возвращаем ответ");
        return $response_json;
    }

    //=========================================================================================
    //=====================================Реклама=============================================
    //=========================================================================================

    /**
     * Запрос групп, в которых пользователь может создать рекламу
     *
     * @return array|null
     */
    public function getAdvertisableGroups()
    {
        $this->_logger->logInfo("[VKApiHelper][getAdvertisableGroups()]");

        $params = array(
            'user_id' => $this->_user_id,
            'filter' => 'advertiser',
            'extended' => true,
        );

        $vk_json = $this->getVKJSON('groups.get', $params);

        if (isset($vk_json['response'])) {
            return $vk_json['response']['items'];
        } else {
            $this->_logger->logError("[VKApiHelper][getAdvertisableGroups()] Ошибка: {1}", $vk_json['error']);
            return null;
        }
    }

    public function getAdsClients()
    {
        $this->_logger->logInfo("[VKApiHelper][getAdsClients()]");

        $params = array(
            'account_id' => intval($this->_account_id),
        );

        $vkjson = $this->getVKJSON('ads.getClients', $params);

        if (isset($vkjson['response'])) {
            return $vkjson['response'];
        } else {
            $this->_logger->logError("[VKApiHelper][getAdsClients()] Ошибка: " . var_export($vkjson['error'], true));
            return null;
        }
    }

    /**
     * Создает скрытую запись, которая не попадает на стену сообщества
     * и в дальнейшем может быть использована для создания рекламного объявления типа "Запись в сообществе".
     *
     * @param integer $group_id ID группы
     * @param string $text Текст сообщения
     * @param array $attachments Вложения
     * @param array $additional_params Дополнительные параметры запроса
     * @return null|int Возвращает ID созданного поста в случае успеха
     */
    public function postAdsStealth($group_id, $text = null, $attachments = null, $additional_params = null)
    {
        $params = array(
            'owner_id' => '-' . $group_id,
            'signed' => 0,
        );

        // Цепляем или нет сообщение
        if (!is_string($text)) {
            $this->_logger->logWarning("[VKApiHelper][postAdsStealth()] \$text не является строкой");
        } else {
            $params['message'] = $text;
        }

        // Цепляем или нет вложения
        if (!is_array($attachments) || empty($attachments)) {
            $this->_logger->logWarning("[VKApiHelper][postAdsStealth()] \$attachments не является массивом или он пустой");
            if ( is_string($attachments) ) {
                $this->_logger->logWarning("[VKApiHelper][postAdsStealth()] \$attachments является строкой");
                $params['attachments'] = $attachments;
            }
        } else {
            $params['attachments'] = implode(",", $attachments); // photo100172_166443618,photo-1_265827614
        }

        if (!isset($params['message']) && !isset($params['attachments'])) {
            $this->_logger->logError("[VKApiHelper][postAdsStealth()] Не установлены основные параметры");
            return null;
        }

        // Цепляем или нет доп. параметры
        if (!isset($additional_params) ||
            !is_array($additional_params) ||
            empty($additional_params)) {
            $this->_logger->logWarning("[VKApiHelper][postAdsStealth()] \$additional_params не является массивом или он пустой");
        } else {
            foreach ($additional_params as $key => $value) {
                $params[$key] = $value;
            }
        }

        $this->_logger->logInfoI("[VKApiHelper][postAdsStealth()] Текст: " . $text);

        $vkjson = $this->getVKJSON("wall.postAdsStealth", $params);

        if (isset($vkjson['error'])) {
            $this->_logger->logError("[VKApiHelper][postAdsStealth()] Ошибка: {1}", $vkjson['error']);
            return $vkjson['error']['error_msg'];
        } else {
            // Возвращаем ID созданного поста
            return intval($vkjson['response']['post_id']);
        }
    }

    /**
     * Редактирует скрытую рекламную запись (не попадает на стену сообщества)
     * используется для рекламного объявления типа "Запись в сообществе".
     *
     * @param integer $group_id ID группы
     * @param string $text Текст сообщения
     * @param array $attachments Вложения
     * @param array $additional_params Дополнительные параметры запроса
     * @return null|int Возвращает 1 в случае успеха
     */
    public function editPostAdsStealth($group_id, $post_id, $message = null, $attachments = null, $additional_params = null)
    {
        $params = array(
            'owner_id' => '-' . $group_id,
            'post_id' => $post_id,
            'signed' => 0,
        );

        // Цепляем или нет сообщение
        if (!is_string($message)) {
            $this->_logger->logWarning("[VKApiHelper][editPostAdsStealth()] \$message не является строкой");
        } else {
            $params['message'] = $message;
        }

        // Цепляем или нет вложения
        if (!is_array($attachments) || empty($attachments)) {
            $this->_logger->logWarning("[VKApiHelper][editPostAdsStealth()] \$attachments не является массивом или он пустой");
            if ( is_string($attachments) ) {
                $this->_logger->logWarning("[VKApiHelper][editPostAdsStealth()] \$attachments является строкой");
                $params['attachments'] = $attachments;
            }
        } else {
            $params['attachments'] = implode(",", $attachments); // photo100172_166443618,photo-1_265827614
        }

        if (!isset($params['message']) && !isset($params['attachments'])) {
            $this->_logger->logError("[VKApiHelper][editPostAdsStealth()] Не установлены основные параметры");
            return null;
        }

        // Цепляем или нет доп. параметры
        if (!isset($additional_params) ||
            !is_array($additional_params) ||
            empty($additional_params)) {
            $this->_logger->logWarning("[VKApiHelper][editPostAdsStealth()] \$additional_params не является массивом или он пустой");
        } else {
            foreach ($additional_params as $key => $value) {
                $params[$key] = $value;
            }
        }

        $this->_logger->logInfoI("[VKApiHelper][editPostAdsStealth()] Текст: " . $message);

        $vkjson = $this->getVKJSON("wall.editAdsStealth", $params);
        $response = $vkjson['response'];
        
        $this->_logger->logInfoI("[VKApiHelper][editPostAdsStealth()] Ответ: " . var_export($response, true));
        // Возвращаем 
        if ($response == 1) {
            return true;
        } else {
            return $vkjson;
        }
    }

    public function createAdSpecification($campaign_id, $ad_format, $category_id, $auditories_id, $cost_type, $cost_per_type, $link_url, $name, $status, $target_settings = null)
    {

        $retargeting_groups = implode(",", $auditories_id);

        $data = array(
            'campaign_id' => $campaign_id,
            'ad_format' => $ad_format,
            'link_url' => $link_url,
            'name' => $name,
            'cost_type' => $cost_type,
            'status' => $status,
            'category1_id' => $category_id,
            'age_restriction' => 0,
            // 'day_limit' => '100',
            'ad_platform' => "all",
            // 'ad_platform_no_wall' => 0,
            // 'ad_platform_no_ad_network' => 0,
            // 'impressions_limit' => 5,
            'retargeting_groups' => $retargeting_groups,

        );

        if (isset($target_settings)) {
            $this->_logger->logInfo("[VKApiHelper][createAdSpecification()] Указаны настройки таргетирования. Применяем.");
            foreach ($target_settings as $key => $value) {
                if (!empty($target_settings[$key])) {

                    if (is_array($target_settings[$key])) {

                        if ($key == "events_retargeting_groups") {
                            $data[$key] = $value;
                        } else {
                            $data[$key] = implode(",", $target_settings[$key]);
                        }
                    } else {
                        $data[$key] = $value;
                    }
                    
                } else {
                    $this->_logger->logInfo("[VKApiHelper][createAdSpecification()] $key пустой");
                }
            }

        }

        switch ($cost_type) {
            case 0:
                // CPC - за переходы
                $data['cpc'] = $cost_per_type;
                break;
            case 1:
                // CPM - за 1000 показов
                $data['cpm'] = $cost_per_type;
                break;
            default:
                $this->_logger->logError("[VKApiHelper][createAdSpecification()] Неверный способ оплаты");
                break;
        }

        $this->_logger->logInfo("[VKApiHelper][createAdSpecification()] Настройки итоговые: " . var_export($data, true));

        return $data;
    }

    public function createAds($ads_data)
    {
        $this->_logger->logInfo("[VKApiHelper][createAds()]");

        // Режем массив по 5 штук

        $ads_data_count = count($ads_data);

        if ($ads_data_count > 5) {
            $this->_logger->logInfoI("[VKApiHelper][createAds()] Больше 5 объявлений, разбиваем");

            // Ответы на создание объявлений
            $responses = array();

            // Куски по 5 объявлений
            $ads_data_parts = array_chunk($ads_data, 5);

            foreach ($ads_data_parts as $ads_data_part) {
                $this->_logger->logInfo("[VKApiHelper][createAds()] Кусок: {1}", $ads_data_part);
                $response = $this->createAds($ads_data_part);
                foreach ($response as $r) {
                    array_push($responses, $r);
                }
            }

            $this->_logger->logInfoI("[VKApiHelper][createAds()] Ответы: {1}", $responses);

            return $responses;

        } else {
            if (defined('PHP_MAJOR_VERSION') && PHP_MAJOR_VERSION >= 7) {

                $json_flags = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_LINE_TERMINATORS;

            } else {
                $json_flags = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
            }

            $data = json_encode($ads_data, $json_flags);

            $params = array(
                'account_id' => intval($this->_account_id),
                'data' => $data,
            );

            $vkjson = $this->getVKJSON('ads.createAds', $params);


            $this->_logger->logInfoI("[VKApiHelper][createAds()] Ответы: " . var_export($vkjson['response'], true));

            return $vkjson['response'];

        }

    }

    /**
     * Запрашивает существующие кампании рекламного кабинета
     *
     * @return array Массив с информацией о существующих компаниях
     */
    public function getAdsCampaigns($client_id = null)
    {
        $this->_logger->logInfo("[VKApiHelper][getAdsCampaigns()]");

        $params = array(
            'account_id' => $this->_account_id,
        );

        if (isset($client_id)) {
            $params['client_id'] = $client_id;
        }

        $vkjson = $this->getVKJSON('ads.getCampaigns', $params);

        return $vkjson['response'];
    }

    /**
     * Запрашивает доступные тематики объявлений
     *
     * @return array Массив с тематиками объявлений и их описанием
     */
    public function getAdsCategories()
    {
        $this->_logger->logInfo("[VKApiHelper][getAdsCategories()]");

        $vkjson = $this->getVKJSON('ads.getCategories', null);

        return $vkjson['response']['v2'];
    }

    /**
     * Возвращает список аудиторий ретаргетинга
     *
     * @param int $extended если 1, в результатах будет указан код для размещения на сайте. Устаревший параметр. Используется только для старых групп ретаргетинга, которые пополнялись без помощи пикселя. Для новых аудиторий его следует опускать.
     *
     * @return array Массив с тематиками объявлений и их описанием
     */
    public function getAdsTargetGroups($client_id = null, $extended = 0)
    {
        $this->_logger->logInfo("[VKApiHelper][getAdsTargetGroups()]");

        $params = array(
            'account_id' => $this->_account_id,
            'extended' => $extended,
        );

        if (isset($client_id)) {
            $params['client_id'] = $client_id;
        }

        $vkjson = $this->getVKJSON('ads.getTargetGroups', $params);

        if (isset($vkjson['response'])) {
            return $vkjson['response'];
        } else {
            $this->_logger->logError("[VKApiHelper][getAdsTargetGroups()] Ошибка: " . var_export($vkjson['error'], true));
            return null;
        }
    }

    /**
     * Возвращает набор подсказок для различных параметров таргетинга
     *
     * @param string $section Раздел, по которому запрашиваются подсказки
     * @return void
     */
    public function getAdsSuggestions($section)
    {
        $this->_logger->logInfo("[VKApiHelper][getAdsSuggestions()]");

        $params = array(
            'section' => $section,
        );

        $vkjson = $this->getVKJSON('ads.getSuggestions', $params);

        if (isset($vkjson['response'])) {
            return $vkjson['response'];
        } else {
            $this->_logger->logError("[VKApiHelper][getAdsSuggestions()] Ошибка: " . var_export($vkjson['error'], true));
            return null;
        }
    }

    //=========================================================================================
    //======================================Группы=============================================
    //=========================================================================================

    /**
     * Запрашивает список фотоальбомов сообщества
     *
     * @return array Ассоциативный массив с информацией о фотоальбомах в сообществе
     */
    public function getGroupPhotoAlbums()
    {
        $this->_logger->logInfo("[VKApiHelper][getGroupPhotoAlbums()]");

        $this->_logger->logInfo("[VKApiHelper][getGroupPhotoAlbums()] Вызываем VKApiHelper::getVKJSON()");

        $vkjson = $this->getVKJSON('photos.getAlbums', array(
            'owner_id' => '-' . $this->_groupId,
        ));

        return $vkjson;
    }

    /**
     * Запрашивает список фотографий в альбоме сообщества
     *
     * @return array Ассоциативный массив с информацией о фотографиях в альбоме
     */
    public function getGroupAlbumPhotos()
    {
        $this->_logger->logInfo("[VKApiHelper][getGroupAlbumPhotos()]");

        $this->_logger->logInfo("[VKApiHelper][getGroupAlbumPhotos()] Вызываем VKApiHelper::getVKJSON()");

        $vkjson = $this->getVKJSON('photos.get', array(
            'owner_id' => '-' . $this->_groupId,
            'album_id' => $this->_albumId,
            'count' => 1000,
            'offset' => 0,
        ));

        return $vkjson;
    }

    /**
     * Запрашивает последнего вступившего в сообщество пользователя
     * @return string ID последнего вступившего в сообщество пользователя
     */
    public function getLastUserID()
    {
        $this->_logger->logInfo("[VKApiHelper][getLastUserID()]");

        $this->_logger->logInfo("[VKApiHelper][getLastUserID()] Вызываем VKApiHelper::getVKJSON()");

        $vkjson = $this->getVKJSON('groups.getMembers', array(
            'group_id' => $this->_groupId,
            'sort' => 'time_desc',
            'count' => 1,
        ));

        $last_user_id = $vkjson['response']['items'][0];

        return $last_user_id;
    }

    /**
     * Запрашивает темы в обсуждениях группы
     * @param string $topic_ids Список тем, информацию о которых необходимо получить. Если не указать - возвращается информация о теме с id _topicId
     * @return array Ассоциативный массив с информацией об темах в обсуждениях группы
     */
    public function getTopics($topic_ids = null)
    {
        $this->_logger->logInfo("[VKApiHelper][getTopics()]");
        $this->_logger->logInfo("[VKApiHelper][getTopics()] Topic ids: " . $topic_ids);
        $params = array(
            'group_id' => $this->_groupId,
            'topic_ids' => $this->_topicId,
        );

        $this->_logger->logInfo("[VKApiHelper][getTopics()] Params: {1}", $params);

        if ($topic_ids) {
            $this->_logger->logInfo("[VKApiHelper][getTopics()] В метод передан параметр \$topic_ids = " . $topic_ids);
            $params['topic_ids'] = $topic_ids;
        }

        $this->_logger->logInfo("[VKApiHelper][getTopics()] Params: {1}", $params);

        $this->_logger->logInfo("[VKApiHelper][getTopics()] Вызываем VKApiHelper::getVKJSON()");

        $vkjson = $this->getVKJSON('board.getTopics', $params);

        return $vkjson;
    }

    /**
     * Запрашивает список сообщений в теме обсуждения
     * @param int $offset Смещение относительно первого сообщения
     * @return array Ассоциативный массив с информацией о сообщениях в теме
     */
    public function getTopicComments($offset)
    {
        $this->_logger->logInfo("[VKApiHelper][getTopicComments()]");
        $this->_logger->logInfo("[VKApiHelper][getTopicComments()] Offset: " . $offset);

        $this->_logger->logInfo("[VKApiHelper][getTopicComments()] Вызываем VKApiHelper::getVKJSON()");

        $vkjson = $this->getVKJSON('board.getComments', array(
            'group_id' => $this->_groupId,
            'count' => 100,
            'topic_id' => $this->_topicId,
            'extended' => true,
            'offset' => $offset,
        ));

        return $vkjson;
    }

    /**
     * Запрашивает информацию о пользователях вместе с ссылками на их фотографии
     * @param string $user_ids Перечисленные через запятую идентификаторы пользователей или их короткие имена (screen_name).
     * @param bool $large Если установлено в true, возвращает ссылку на большую фотографию в поле 'photo_200'
     * @return array Массив объектов пользователей. В каждом объекте будет свойство 'photo_100' или 'photo_200', содержащее ссылку на фото
     */
    public function getUsersInfoWithPhoto($user_ids, $large = false)
    {
        $this->_logger->logInfo("[VKApiHelper][getUsersInfoWithPhoto()]");
        $this->_logger->logInfo("[VKApiHelper][getUsersInfoWithPhoto()] User ids: " . $user_ids);
        $this->_logger->logInfo("[VKApiHelper][getUsersInfoWithPhoto()] Нужно большое фото?: " . $large);

        $params = array(
            'user_ids' => $user_ids,
            'fields' => 'photo_100',
        );

        if ($large) {
            $params['fields'] = 'photo_200';
        }

        $this->_logger->logInfo("[VKApiHelper][getUsersInfoWithPhoto()] Итоговые параметры: {1}", $params);

        $this->_logger->logInfo("[VKApiHelper][getUsersInfoWithPhoto()] Вызываем VKApiHelper::getVKJSON()");

        $vkjson = $this->getVKJSON('users.get', $params);

        return $vkjson;
    }

    /**
     * Запрашивает URL-адрес для загрузки обложки сообщества
     *
     * @return array Массив с ключом 'upload_url', в котором хранится адрес сервера для загрузки фото
     */
    public function getGroupCoverUploadURL()
    {
        $this->_logger->logInfo("[VKApiHelper][getGroupCoverUploadURL()]");

        $params = array(
            'group_id' => $this->_groupId,
            'crop_x2' => '1590',
        );

        $this->_logger->logInfo("[VKApiHelper][getGroupCoverUploadURL()] Вызываем VKAPI::api_call()");

        return $this->_api->api_call('photos.getOwnerCoverPhotoUploadServer', $params);
    }

    /**
     * Сохраняет изображение для обложки сообщества после его успешной загрузки на сервер
     * @param string $hash Параметр hash полученный в результате загрузки изображения на сервер
     * @param string $photo Параметр photo полученный в результате загрузки фотографии на сервер
     * @return array Массив images объектов, описывающих копии загруженной фотографии
     */
    public function saveGroupCoverPhoto($hash, $photo)
    {
        $this->_logger->logInfo("[VKApiHelper][saveGroupCoverPhoto()]");
        $this->_logger->logInfo("[VKApiHelper][saveGroupCoverPhoto()] Hash: " . $hash);
        $this->_logger->logInfo("[VKApiHelper][saveGroupCoverPhoto()] Photo: " . $photo);

        $params = array(
            'hash' => $hash,
            'photo' => $photo,
        );

        $this->_logger->logInfo("[VKApiHelper][saveGroupCoverPhoto()] Вызываем VKAPI::raw_api_call()");

        return $this->_api->raw_api_call('photos.saveOwnerCoverPhoto', $params);
    }

    public function getGroupPollById($poll_id)
    {
        $this->_logger->logInfo("[VKApiHelper][getGroupPollById()]");
        $this->_logger->logInfo("[VKApiHelper][getGroupPollById()] Вызываем VKApiHelper::getVKJSON()");

        $vkjson = $this->getVKJSON('polls.getById', array(
            'owner_id' => '-' . $this->_groupId,
            'poll_id' => $poll_id,
        ));

        return $vkjson;
    }

    public function getGroupPollVoters($poll_id, $answers, $offset)
    {
        $this->_logger->logInfo("[VKApiHelper][getGroupPollVoters()]");
        $this->_logger->logInfo("[VKApiHelper][getGroupPollVoters()] Poll id: " . $poll_id);
        $this->_logger->logInfo("[VKApiHelper][getGroupPollVoters()] Answers: " . $answers);
        $this->_logger->logInfo("[VKApiHelper][getGroupPollVoters()] Offset: " . $offset);

        $this->_logger->logInfo("[VKApiHelper][getGroupPollVoters()] Вызываем VKApiHelper::getVKJSON()");
        $vkjson = $this->getVKJSON('polls.getVoters', array(
            'owner_id' => '-' . $this->_groupId,
            'answer_ids' => $answers,
            'poll_id' => $poll_id,
            'offset' => $offset,
            'count' => 1000,
        ));

        return $vkjson;
    }
    

    //=========================================================================================
    //===================================База данных===========================================
    //=========================================================================================

    public function getCountries($need_all = 0)
    {

        $params = array(
            'need_all' => $need_all,
            'count' => 1000,
        );

        $vkjson = $this->getVKJSON('database.getCountries', $params); 

        return $vkjson['response']['items'];
    }

    public function getRegions($country_id)
    {
        $this->_logger->logInfo("[VKApiHelper][getRegions()]");

        $params = array(
            'country_id' => $country_id,
            'count' => 1000,
        );

        $vkjson = $this->getVKJSON('database.getRegions', $params);

        return $vkjson['response']['items'];
    }

    public function getCities($country_id, $region_id = null, $need_all = 0)
    {
        $this->_logger->logInfo("[VKApiHelper][getCities()]");

        $params = array(
            'country_id' => $country_id,
            'need_all' => $need_all,
            'count' => 1000,
        );

        if (isset($region_id)) {
            $params['region_id'] = $region_id;
        }

        $vkjson = $this->getVKJSON('database.getCities', $params);

        return $vkjson['response']['items'];
    }


    //============================= СКРИПТЫ ============================

    public function getInfoAboutPost($post_id)
    {
        $this->_logger->logInfo("[VKApiHelper][getInfoAboutPost()]");
        $this->_logger->logInfo("[VKApiHelper][getInfoAboutPost()] Post ID: " . $post_id);

        $params = array(
            'group_id' => $this->_groupId,
            'post_id' => $post_id,
        );

        $this->_logger->logInfo("[VKApiHelper][getInfoAboutPost()] Вызываем VKAPIScriptExecutor::getInfoAboutPost()");

        $script_response = $this->_scriptExecutor->getInfoAboutPost($params);

        return $script_response;
    }

    public function getPollsVoters($poll_id)
    {
        $this->_logger->logInfo("[VKApiHelper][getPollsVoters()]");
        $this->_logger->logInfo("[VKApiHelper][getPollsVoters()] Poll ID: " . $poll_id);

        $params = array(
            'group_id' => $this->_groupId,
            'poll_id' => $poll_id,
        );

        $this->_logger->logInfo("[VKApiHelper][getPollsVoters()] Вызываем VKAPIScriptExecutor::getPollsVoters()");

        $script_response = $this->_scriptExecutor->getPollsVoters($params);

        return $script_response;
    }

    public function getPosts($start)
    {
        $this->_logger->logInfo("[VKApiHelper][getPosts()]");
        $this->_logger->logInfo("[VKApiHelper][getPosts()] Начиная с: " . $start);

        $params = array(
            'group_id' => $this->_groupId,
            'start' => strtotime($start),
        );

        $this->_logger->logInfo("[VKApiHelper][getPosts()] Вызываем VKAPIScriptExecutor::getPosts()");

        $script_response = $this->_scriptExecutor->getPosts($params);

        return $script_response;
    }

    //============================= ТОКЕН ============================

    public static function buildTokenURL_Explicit($app_id)
    {
        $logger = new Logger('VKApiHelper');
        $logger->logInfo("[VKApiHelper][buildTokenURL()]");
        $logger->logInfo("[VKApiHelper][buildTokenURL()] App ID: " . $app_id);

        $params = array(
            'client_id' => $app_id,
            'redirect_uri' => 'https://oauth.vk.com/blank.html',
            'response_type' => 'token',
            'scope' => 'photos,wall,groups,stats,offline',
        );

        $url = 'https://oauth.vk.com/authorize?' . http_build_query($params);

        $logger->logInfo("[VKApiHelper][buildTokenURL()] URL: " . $url);

        return $url;
    }

    public static function buildCodeUrl($app_id, $app_secret)
    {

        global $server_name;
        global $port;

        $logger = new Logger('VKApiHelper');
        $logger->logInfo("[VKApiHelper][buildCodeUrl()]");
        $logger->logInfo("[VKApiHelper][buildCodeUrl()] App ID: " . $app_id);
        $logger->logInfo("[VKApiHelper][buildCodeUrl()] App secret: " . $app_secret);

        $redirect_script = 'http://' . $server_name . ':' . $port . $_SERVER['PHP_SELF'];

        $redirect_params = array(
            'app_id' => $app_id,
            'app_secret' => $app_secret,
        );

        $redirect_uri = $redirect_script . "?" . http_build_query($redirect_params);

        $params = array(
            'client_id' => $app_id,
            'redirect_uri' => $redirect_uri,
            'response_type' => 'code',
            'v' => '5.83',
            'scope' => 'photos,wall,groups,stats,offline,ads',
        );

        $url = 'https://oauth.vk.com/authorize?' . http_build_query($params);

        $logger->logInfo("[VKApiHelper][buildCodeUrl()] URL: " . $url);

        return $url;
    }

    private static function buildTokenURL($app_id, $app_secret, $code)
    {
        global $server_name;
        global $port;

        $logger = new Logger('VKApiHelper');
        $logger->logInfo("[VKApiHelper][buildTokenURL()]");
        $logger->logInfo("[VKApiHelper][buildTokenURL()] App ID: " . $app_id);
        $logger->logInfo("[VKApiHelper][buildTokenURL()] App Secret: " . $app_secret);
        $logger->logInfo("[VKApiHelper][buildTokenURL()] Code: " . $code);

        $redirect_script = 'http://' . $server_name . ':' . $port . $_SERVER['PHP_SELF'];

        $redirect_params = array(
            'app_id' => $app_id,
            'app_secret' => $app_secret,
        );

        $redirect_uri = $redirect_script . "?" . http_build_query($redirect_params);

        $params = array(
            'client_id' => $app_id,
            'client_secret' => $app_secret,
            'redirect_uri' => $redirect_uri,
            'code' => $code,
        );

        $logger->logInfoI("[VKApiHelper][buildTokenURL()] Параметры: " . var_export($params, true));

        $url = 'https://oauth.vk.com/access_token?' . http_build_query($params);

        $logger->logInfo("[VKApiHelper][buildTokenURL()] URL: " . $url);

        return $url;
    }

    public static function getToken($app_id, $client_secret, $code)
    {

        $logger = new Logger('VKApiHelper');
        $logger->logInfo("[VKApiHelper][getToken()]");
        $logger->logInfo("[VKApiHelper][getToken()] App ID: " . $app_id);
        $logger->logInfo("[VKApiHelper][getToken()] Code: " . $code);
        $logger->logInfo("[VKApiHelper][getToken()] Client secret: " . $client_secret);

        $logger->logInfo("[VKApiHelper][getToken()] Вызываем VKApiHelper::buildTokenURL()");

        $url = VKApiHelper::buildTokenURL($app_id, $client_secret, $code);

        $logger->logInfo("[VKApiHelper][getToken()] Инициализируем и настраиваем сессию cURL");
        // Настраиваем cURL
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $logger->logInfo("[VKApiHelper][getToken()] Выполняем запрос с помощью cURL");
        // Выполняем cURL - запрос
        $raw_response = curl_exec($ch);

        $logger->logInfo("[VKApiHelper][getToken()] Получен ответ: " . $raw_response);
        $logger->logInfo("[VKApiHelper][getToken()] Парсим ответ с помощью json_decode");

        // Парсим ответ
        $decoded_response = json_decode($raw_response, true);

        $logger->logInfoI("[VKApiHelper][getToken()] Декодированный ответ: " . var_export($decoded_response, true));

        $access_token = $decoded_response['access_token'];

        // Если в ответе содержится токен
        if ($access_token) {
            $logger->logInfo("[VKApiHelper][getToken()] Токен: " . $access_token);

            return $access_token;
        } else {
            $logger->logError("[VKApiHelper][getToken()] Токен не получен!");

            return null;
        }

    }

    public static function saveToken($access_token)
    {
        $logger = new Logger('VKApiHelper');
        $logger->logInfo("[VKApiHelper][saveToken()]");
        $logger->logInfo("[VKApiHelper][saveToken()] Token: " . $access_token);

        $token_setting = array('vkapi' => array('access_token' => $access_token));
        $logger->logInfo("[VKApiHelper][saveToken()] Массив параметров: {1}", $token_setting);

        $logger->logInfo("[VKApiHelper][saveToken()] Вызываем update_settings()");
        update_settings($token_setting);
    }

    public static function saveAdsAccountId($account_id, $agency = "false")
    {
        $logger = new Logger('VKApiHelper');
        $logger->logInfo("[VKApiHelper][saveAdsAccountId()]");
        $logger->logInfo("[VKApiHelper][saveAdsAccountId()] ID: " . $account_id);
        $logger->logInfo("[VKApiHelper][saveAdsAccountId()] Agency: {1}", $agency);

        $token_setting = array('vkapi' => array('account_id' => $account_id, 'agency' => $agency, ));
        $logger->logInfo("[VKApiHelper][saveAdsAccountId()] Массив параметров: {1}", $token_setting);

        $logger->logInfo("[VKApiHelper][saveAdsAccountId()] Вызываем update_settings()");
        update_settings($token_setting);
    }

    public static function updateGroupId($group_id)
    {
        $logger = new Logger('VKApiHelper');
        $logger->logInfo("[VKApiHelper][updateGroupId()]");
        $logger->logInfo("[VKApiHelper][updateGroupId()] ID: " . $group_id);

        $token_setting = array('vkapi' => array('group_id' => $group_id));
        $logger->logInfo("[VKApiHelper][updateGroupId()] Массив параметров: " . var_export($token_setting, true));

        $logger->logInfo("[VKApiHelper][updateGroupId()] Вызываем update_settings()");
        update_settings($token_setting);
    }

}
