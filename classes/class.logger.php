<?php
class Logger
{
    // Режимы вывода
    const MODE_ALL = 0;
    const MODE_WARN = 1;
    const MODE_ERR = 2;

    private $_logpath = BASEPATH . 'logs'; // Путь до папки с логами
    private $_prefix = '';
    private $_output_target = 'file';
    private $_mode = Logger::MODE_WARN; // 0 - MODE_ALL, 1 - MODE_WARN, 2 - MODE_ERR

    public function __construct($prefix = '', $output_target = 'file', $mode = Logger::MODE_WARN)
    {
        // Если папка не обнаружена
        if (!is_dir($this->_logpath)) {
            $dir_message = "[Logger][__construct()] Папка с логами не обнаружена, создаем...";
            // Выводим сообщение
            echo $dir_message . "<br>";
            // Создаем папку
            if ($this->createLogDir()) {
                // Пишем о произошедшем в лог
                $dir_was_created_message = "[Logger][__construct()] Папка с логами " . $this->_logpath . " отсутствовала и была создана";
                $this->logWarning($dir_was_created_message);
            } else {
                // Иначе завершаем работу
                return;
            }
        }
        $logger_initialized_message = "[Logger][__construct()] Объект класса Logger успешно создан с параметрами PREFIX = " . $prefix . "; MODE = " . $mode . "; OUTPUT_TARGET = " . $output_target;

        $this->_prefix = $prefix;
        $this->_mode = $mode;
        $this->_output_target = $output_target;

        $this->logInfo($logger_initialized_message);
    }

    /**
     * Записывает строку сообщения с пометкой [INFO] в сегодняшний лог файл
     *
     * @param string $message Сообщение
     * @param string $log_message_variables Переменные для отображения в сообщении
     */
    public function logInfo($message, ...$log_message_variables)
    {
        if ($this->_mode > Logger::MODE_ALL) {
            // Если режим логгера выше чем 0 (MODE_ALL) то нихуя не выводим и не пишем
            return;
        }
        $info_message = '[INFO]' . $message;
        $this->writeMessage($info_message, $log_message_variables);
    }
    /**
     * Записывает строку сообщения с пометкой [INFO] в сегодняшний лог файл
     *
     * @param string $message Сообщение
     * @param string $log_message_variables Переменные для отображения в сообщении
     */
    public function logInfoI($message, ...$log_message_variables)
    {
        $info_message = '[INFO]' . $message;
        $this->writeMessage($info_message, $log_message_variables);
    }

    /**
     * "Важная" версия logInfo()
     * Записывает строку сообщения с пометкой [WARN] в сегодняшний лог файл
     *
     * @param string $warning_description Сообщение предупреждения
     * @param string $log_message_variables Переменные для отображения в сообщении
     */
    public function logWarning($warning_description, ...$log_message_variables)
    {
        if ($this->_mode > Logger::MODE_WARN) {
            // Если режим логгера выше чем 1 (MODE_WARN) то нихуя не выводим и не пишем
            return;
        }
        $warning_message = '[WARN]' . $warning_description;
        $this->writeMessage($warning_message, $log_message_variables);
    }
    /**
     * "Важная" версия logWarning()
     * Записывает строку сообщения с пометкой [WARN] в сегодняшний лог файл
     *
     * @param string $warning_description Сообщение предупреждения
     * @param string $log_message_variables Переменные для отображения в сообщении
     */
    public function logWarningI($warning_description, ...$log_message_variables)
    {
        $warning_message = '[WARN]' . $warning_description;
        $this->writeMessage($warning_message, $log_message_variables);
    }

    /**
     * Записывает строку сообщения с пометкой [ERROR] в сегодняшний лог файл
     *
     * @param string $error_description Сообщение ошибки
     * @param string $log_message_variables Переменные для отображения в сообщении
     */
    public function logError($error_description, ...$log_message_variables)
    {
        $error_message = '[ERROR]' . $error_description;
        $this->writeMessage($error_message, $log_message_variables);
    }

    /**
     * Формирует и записывает итоговую строку в файл
     *
     * @param string $log_message Строка сообщения
     * @param string $log_message_variables Переменные для отображения в сообщении. Если не указано - используется значение по умолчанию
     */
    private function writeMessage($log_message, $log_message_variables)
    {
        $output_target = null;
        // Считаем плейсхолдеры 
        $placeholders_count = $this->countPlaceholders($log_message);

        // Если количество плейсхолдеров равно количество переменных
        // Вставляем переменные на места плейсхолдеров
        if (count($log_message_variables) == $placeholders_count) {
            $log_message = $this->replaceVariablePlaceholders($log_message, $log_message_variables);
        } else if (count($log_message_variables) == $placeholders_count + 1) {
            // Если количество переменных превышает количество плейсхолдеров на 1 - значит последний параметр есть $output_target
            $output_target = end($log_message_variables);
        } 
        // Добавляем метку со временем
        $timemarked_message = $this->addTimeMark($log_message);

        // Завершаем сообщение переносом строки
        $final_message = $timemarked_message . "\r\n";

        $actual_output_target = '';

        if ($output_target && $output_target != '') {
            $actual_output_target = $output_target;
        } else {
            $actual_output_target = $this->_output_target;
        }

        switch ($actual_output_target) {
            case 'file':
                $this->writeMessageToFile($final_message);
                break;
            case 'browser':
                $this->writeMessageToBrowser($final_message);
                break;
            case 'both':
                $this->writeMessageToFile($final_message);
                $this->writeMessageToBrowser($final_message);
                break;
            default:
                $this->writeMessageToFile($final_message);
                break;
        }
    }

    //======================================================================================================
    //=====================================PRIVATE METHODS==================================================
    //======================================================================================================

    private function writeMessageToFile($message)
    {
        // Считываем дату в текущем часовом поясе
        $current_date = new DateTime('now');
        $current_date_string = $current_date->format('Y-m-d');

        // Имя файла формата 1999-09-30.txt
        $logfile_name = $current_date_string . ".txt";

        // Если установлен префикс - добавляем его в начало имени файла
        if ($this->_prefix != '') {
            $logfile_name = "[" . $this->_prefix . "] " . $logfile_name;
        }
        $logfile_path = $this->_logpath . "/" . $logfile_name;

        // Пишем итоговое сообщение в файл
        file_put_contents($logfile_path, $message, FILE_APPEND | LOCK_EX);
    }

    private function writeMessageToBrowser($message)
    {
        echo $message . "<br>";
    }

    /**
     * Добавляет к сообщению метку со временем
     *
     * @param string $message Исходное сообщение
     * @return string Исходное сообщение с добавленной в начале меткой со временем
     */
    private function addTimeMark($message)
    {

        // Считываем дату в текущем часовом поясе
        $current_date = new DateTime('now');

        // Вытаскиваем часы минуты и секунды в формате 02:05:01
        $current_time_string = $current_date->format('H:i:s');

        // Добавляем время квадратных скобках к началу сообщения
        $timemarked_message = "[" . $current_time_string . "]" . $message;

        return $timemarked_message;

    }

    /**
     * Считает плейсхолдеры в сообщении
     *
     * @param string $message Сообщение с плейсхолдерами
     * @return integer Количество плейсхолдеров 
     */
    public function countPlaceholders($message)
    {
        // Регулярка для обнаружения {1}, {2},...,{39523}, etc 
        $placeholder_regexp = "#\{\d+\}#";
        
        $placeholders = array();
        
        // Вытаскиваем все плейсхолдеры из сообщения
        preg_match_all($placeholder_regexp, $message, $placeholders);

        return count($placeholders[0]);
    }

    /**
     * Заменяет плейсхолдеры в сообщении на значения переменных
     *
     * @param string $message Сообщение с плейсхолдерами
     * @param array $replacements Массив переменных
     * @return string Сообщение с замененными плейсхолдерами
     */
    public function replaceVariablePlaceholders($message, $replacements)
    {
        foreach ($replacements as $key => $value) {

            // Плейсхолдер типа {1}, {2}, {3}
            $placeholder = "\{" . ($key + 1) . "\}";

            if (is_array($value) || is_object($value) || is_bool($value)) {
                $replacement = json_encode($value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            } else {
                $replacement = $value;
            }

            // Заменяем плейсхолдеры на значение переменной
            $message = mb_ereg_replace($placeholder, $replacement, $message);

        }

        return $message;
    }

    /**
     * Создает папку для хранение лог-файлов
     *
     * @return boolean Успешность создания папки. `TRUE` если папка успешно создана
     */
    private function createLogDir()
    {
        $success = mkdir($this->_logpath);
        $status_message = "[Logger][createLogDir()]";

        if ($success) {
            $status_message = "[Logger][createLogDir()] Папка с логами успешно создана";

        } else {
            $status_message = "[Logger][createLogDir()] Не удалось создать папку " . $this->_logpath;
        }

        echo $status_message . "<br>";

        return $success;
    }
}