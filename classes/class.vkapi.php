<?php

require_once BASEPATH . "/classes/class.logger.php";

class VKAPI
{

    private $_accessToken; // Токен, который используем для запросов
    private $_apiUrl = 'https://api.vk.com/method/';
    private $_apiV = '5.83';
    private $_logger = null;

    public function __construct($accessToken)
    {
        $this->_logger = new Logger('VKAPI', 'file');
        $this->_logger->logInfo("[VKAPI][__construct()]");

        $this->_logger->logInfo("[VKAPI][__construct()] Access token: " . $accessToken);
        $this->_accessToken = $accessToken;

        $this->_logger->logInfo("[VKAPI][__construct()] Объект класса VKAPI успешно создан");
    }

    /**
     * Производит запрос к VK API и возвращает ассоциативный массив
     * @param string $method Имя метода VK API
     * @param array $params Массив передаваемых параметров
     * @return array Ассоциативный массив, полученный после декодирования ответа
     */
    public function api_call($method, $params = array())
    {
        $this->_logger->logInfo("[VKAPI][api_call()]");
        $this->_logger->logInfo("[VKAPI][api_call()] Method: " . $method);
        $this->_logger->logInfo("[VKAPI][api_call()] Params: {1}", $params);

        $this->_logger->logInfo("[VKAPI][api_call()] Вызываем VKAPI::raw_api_call()");

        $raw_response = $this->raw_api_call($method, $params);

        $responseObj = json_decode($raw_response, true);

        $this->_logger->logInfo("[VKAPI][api_call()] Декодированный JSON от VK: {1}", $responseObj);

        return $responseObj;

    }

    /**
     * Производит запрос к VK API и возвращает результат запроса
     * @param string $method Имя метода VK API
     * @param array $params Массив передаваемых параметров
     * @return string Ответ полученный в результате запроса
     */
    public function raw_api_call($method, $params = array())
    {

        $this->_logger->logInfo("[VKAPI][raw_api_call()]");
        $this->_logger->logInfo("[VKAPI][raw_api_call()] Method: " . $method);
        $this->_logger->logInfo("[VKAPI][raw_api_call()] Params: {1}", $params);
        $params['access_token'] = $this->_accessToken;
        $params['v'] = $this->_apiV;

        $queryUrl = $this->_apiUrl . $method;

        $this->_logger->logInfo("[VKAPI][raw_api_call()] Query url: " . $queryUrl);

        $this->_logger->logInfo("[VKAPI][raw_api_call()] Вызываем VKAPI::performPostRequest()");
        $raw_response = $this->performPostRequest($queryUrl, $params);

        $this->_logger->logInfo("[VKAPI][raw_api_call()] Ответ от VK: {1}", $raw_response);

        return $raw_response;
    }

    /**
     * Выполняет POST-запрос к указанному адресу
     * @param string $url Адрес запроса
     * @param string $postfields Тело POST запроса
     * @return string Полученный ответ после запроса
     */
    private function performPostRequest($url, $postfields)
    {

        $this->_logger->logInfo("[VKAPI][performPostRequest()]");
        $this->_logger->logInfo("[VKAPI][performPostRequest()] URL: " . $url);
        $this->_logger->logInfo("[VKAPI][performPostRequest()] POST fields: {1}", $postfields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

        if (isset($GLOBALS['debug']) && $GLOBALS['debug']) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        $this->_logger->logInfo("[VKAPI][performPostRequest()] Вызываем curl_exec()");
        $result = curl_exec($ch);

        if ($result === false) {
            $curl_error = curl_error($ch);
            $curl_info = curl_getinfo($ch);

            $this->_logger->logError("[VKAPI][performPostRequest()] Ошибка curl: " . $curl_error);
            $this->_logger->logError("[VKAPI][performPostRequest()] Curl info: {1}", $curl_info);
        } else {
            $this->_logger->logInfo("[VKAPI][performPostRequest()] POST-запрос к VK API успешно выполнен");
        }

        curl_close($ch);
        return $result;
    }
}
