<?php

require_once BASEPATH . "/classes/class.vkapi.php";
require_once BASEPATH . "/classes/class.logger.php";

class VKAPIScriptExecutor {

    private $_api; // Объект класса VKAPI
    private $_scriptsDirectory = BASEPATH . 'vkscripts';
    private $_logger = null;

    public function __construct(VKAPI $api) {
        $this->_logger = new Logger('VKAPIScriptExecutor');
        $this->_logger->logInfo("[VKAPIScriptExecutor][__construct()]");
        $this->_logger->logInfo("[VKAPIScriptExecutor][__construct()] api: " . var_export($api, true) );
        $this->_api = $api;
    }

    /**
     * Магический метод, с помощью которого вызываем скрипт VKScript по его имени
     * @param string $methodName Имя метода, совпадает с именем скрипта
     * @param array $arguments Аргументы, передаваемые функции
     * @return array Ответ от VK API в виде ассоциативного массива
     */
    public function __call($methodName, $arguments) {
        $this->_logger->logInfo("[VKAPIScriptExecutor][__call()]");
        $this->_logger->logInfo("[VKAPIScriptExecutor][__call()] Method name: " . $methodName);
        $this->_logger->logInfo("[VKAPIScriptExecutor][__call()] Вызываем VKAPIScriptExecutor::getScript()");

        $script = $this->getScript($methodName);

        // print("[VKAPIScriptExecutor:__call()] Unprepared script: " . $script);

        $preparedScript = $script;

        if ( count($arguments) && 
            is_array($arguments[0]) ) {
                $this->_logger->logInfo("[VKAPIScriptExecutor][__call()] Передан массив параметров. Подготавливаем скрипт с помощью VKAPIScriptExecutor::prepareParams()");
                $params = $arguments[0];
                $preparedScript =  $this->prepareParams($preparedScript, $params);
        }

        // print("[VKAPIScriptExecutor:__call()] Prepared script: " . $preparedScript);

        $this->_logger->logInfo("[VKAPIScriptExecutor][__call()] Вызываем VKAPI::api_call()");
        $response = $this->_api->api_call('execute', array('code' => $preparedScript) );

        return $response;
    }

    /**
     * Ищет файл скрипта с указанным именем и расширением .vks и возвращает его содержимое в формате `string`
     * @param string $name Имя скрипта
     * @return string|null Содержимое файла или `null` в случае если файл не найден
     */
    private function getScript($name)
    {
        $this->_logger->logInfo("[VKAPIScriptExecutor][getScript()]");
        $this->_logger->logInfo("[VKAPIScriptExecutor][getScript()] Script name: " . $name);
        $filePath = $this->_scriptsDirectory . '/' . $name . '.vks';
        $this->_logger->logInfo("[VKAPIScriptExecutor][getScript()] Ожидаемый путь к файлу скрипта: " . $filePath);
        if ( is_file($filePath) ) {
            $script = file_get_contents($filePath);
            return $script;
        }
        $this->_logger->logInfo("[VKAPIScriptExecutor][getScript()] Содержимое файла скрипта: " . $script);
        return null;
    }

    /**
     * Вписывает значения параметров в холдеры типа %PARAM% в скрипте
     * @param string $script Скрипт в формате `string` 
     * @param array $params Ассоциативный массив параметров. Имена ключей должны совпадать с именами холдеров.
     * @return string Подготовленный скрипт
     */
    private function prepareParams($script, $params)    
    {
        $this->_logger->logInfo("[VKAPIScriptExecutor][prepareParams()]");
        $this->_logger->logInfo("[VKAPIScriptExecutor][prepareParams()] Скрипт: " . $script);
        $this->_logger->logInfo("[VKAPIScriptExecutor][prepareParams()] Параметры: " . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_UNICODE));
        
        $preparedScript = $script;
        
        $this->_logger->logInfo("[VKAPIScriptExecutor][prepareParams()] Проходимся циклом по параметрам");

        foreach ($params as $key => $value ) {
            $paramname = strtoupper($key);
            $this->_logger->logInfo("[VKAPIScriptExecutor][prepareParams()] Заменяем " . "%" . $paramname . "% на " . $value);
            $preparedScript = str_replace(      '%' . $paramname. '%', 
                                                        $value,
                                                        $preparedScript);
        }

        $this->_logger->logInfo("[VKAPIScriptExecutor][prepareParams()] Итоговый скрипт: " . $preparedScript);

        return $preparedScript;
    }
}

?>