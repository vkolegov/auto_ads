<?php

require_once BASEPATH . "/basic_functions.php";
require_once BASEPATH . "/classes/class.logger.php";

/**
 * Класс-враппер класса PDO
 *
 * Используется для инициализации и работы с БД
 * 
 * При создании экземпляра считывает настройки из файла
 */
class db extends PDO
{
    private $dbo = array(
        PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND    => 'SET NAMES \'UTF8\''
    );
    private $_logger = null;

    /**
     * Создает экземпляр класса
     * Инициализирует подключение к БД используя файл настроек
     * @param string $dbsection Раздел файла настроек, задается как [section]
     * @param string $filepath Путь к файлу настроек относительно корневой директории
     */
    function __construct($dbsection = 'database') {
        
        $this->_logger = new Logger('db');
        $this->_logger->logInfo("[db][__construct()]");
        $this->_logger->logInfo("[db][__construct()] Раздел файла: " . $dbsection);

        $this->_logger->logInfo("[db][__construct()] Вызываем db::readSettings()");

        $settings = $this->readSettings($dbsection);
        
        $this->_logger->logInfo("[db][__construct()] Настройки подключения: " . json_encode($settings, JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_UNICODE) );

        $this->_logger->logInfo("[db][__construct()] Вызываем PDO::__construct()");

        parent::__construct($settings['dsn'],
                            $settings['username'],
                            $settings['password'],
                            $this->dbo)
                            ;
        $this->_logger->logInfo("[db][__construct()] Объект класса db успешно создан");
    }

    /**
     * Считывает параметры подключения из файла настроек
     * @param string $dbsection Раздел файла с настройками
     * @param string $filepath Путь к файлу относительно основной директории
     * @return array Массив с настройками
     */
    function readSettings($dbsection) {
        $this->_logger->logInfo("[db][readSettings()]");
        $this->_logger->logInfo("[db][readSettings()] Раздел файла: " . $dbsection);

        $this->_logger->logInfo("[db][readSettings()] Вызываем read_settings()");
        $db_settings = read_settings($dbsection);
        
        $pdo_settings = array(  'dsn'       => '',
                            'username'  => '',
                            'password'  => ''
                        );
        
        $pdo_settings['dsn']        = $db_settings['driver'] .
        ':host='                . $db_settings['host'] .
        ';dbname='              . $db_settings['schema'] . 
        ';charset='             . $db_settings['charset'];

        $pdo_settings['username']   = $db_settings['username'];
        $pdo_settings['password']   = $db_settings['password'];

        return $pdo_settings;

    }
}


?>