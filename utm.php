<?php


function generateUtmParams($source, $medium, $campaign, $content) {

    $utm_params = array(
        'utm_source' => $source,
        'utm_medium' => $medium,
        'utm_campaign' => $campaign,
        'utm_content' => $content,
    );

    return http_build_query($utm_params);
}

?>