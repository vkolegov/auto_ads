<?php
// Директория
define('BASEPATH', str_replace('\\', '/', dirname(__FILE__)) . '/');
// Часовой пояс
date_default_timezone_set('Europe/Moscow');
// Кодировка
mb_internal_encoding("UTF-8");
// Путь к конфигу
$config_path = BASEPATH.'config';
// Имя сервера (адрес сайта)
$server_name = $_SERVER['SERVER_NAME'];
$port = '80';
//print $output_cover;
// Начало сессии
session_start();
?>