<?php

require_once 'predefined_arrays.php';

//
// Генерирование html
//

function generateOptionsHTML($arr, $value_key = 'id', $title_key = 'title', $append_to_title = '', $prepend_to_title='', $preselect_by_value='') 
{
    $html = "";
    foreach ($arr as $key => $element) {
        if ($value_key == 'key') {
            $value = $key;
        } else {
            $value = $element[$value_key];
        }
        if ($title_key == 'element') {
            $title = $element;
        } else {
            $title = $element[$title_key];
        }

        //save_retargeting_groups
        $option_dom = "<option value= \"$value\"";
                        if ($preselect_by_value == $value) {
                            $option_dom = $option_dom . " selected";
                        }
                        if ($append_to_title !== '') {
                            $title = $title . " (" . $element[$append_to_title] . ")";
                        }

                        if ($prepend_to_title !== '') {
                            $title = "(" . $element[$prepend_to_title] . ") " . $title;
                        }
                        $option_dom = $option_dom . ">$title</option>";
                        $html = $html . $option_dom;
    }

    return $html;
}

function generateSelectHTML($filename, $options_array)
{
    $html = file_get_contents($filename);

    $options_array_count = count($options_array);

    for ($i=0; $i < $options_array_count; $i++) { 
        $html = mb_ereg_replace("{options_" . ($i+1) . "}", $options_array[$i], $html);
    }

    return $html;
}

?>