<?php
/**
 * Читает файл настроек
 * 
 * @param string $section Настройки какого раздела файла вернуть. Если не указана - возвращаются все разделы с настройками
 * @return array Ассоциативный массив считанных настроек [секция1] => array('параметр1' => значение)
 */
function read_settings($section = null) {
    global $config_path;

    if (!$settings = parse_ini_file($config_path, true)) {
        throw new Exception("Невозможно открыть " . $config_path, 1);
    }

    if ($section && $section != '') {
        return $settings[$section];
    } else {
        return $settings;
    }
}

function update_settings($new_settings) 
{
    // global $global_logger;

    // $global_logger->logInfo("[function.php][update_settings()] Вызываем read_settings()");

    $old_settings = read_settings();
    $updated_settings = $old_settings;

    // $global_logger->logInfo("[function.php][update_settings()] Проходимся по новому массиву настроек");
    foreach ($new_settings as $key => $new_val) {
        if (is_array($new_val)) {

            $section = $new_val;
            $section_name = $key;
            // $global_logger->logInfo("[function.php][update_settings()] В новых настройках найдена секция " . $section_name);

            if (!is_array($old_settings[$section_name])) {
                // $global_logger->logWarning("[function.php][update_settings()] В старых настройках секция $section_name не найдена. Добавляем... ");
                $updated_settings[$section_name] = $section;
            }
            // $global_logger->logInfo("[function.php][update_settings()] Проходимся по настройкам секции $section_name ");
            foreach ($section as $sec_key => $new_sec_value) {
                $old_value = $old_settings[$section_name][$sec_key];

                // $global_logger->logInfo("[function.php][update_settings()] Заменяем в секции $section_name значение $sec_key с $old_value на $new_sec_value");
                $updated_settings[$section_name][$sec_key] = $new_sec_value;
            }

        } else {
            $old_value = $old_settings[$key];
            // $global_logger->logInfo("[function.php][update_settings()] Заменяем значение $key с $old_value на $new_val");
            $updated_settings[$key] = $new_val;
        }
    }

    write_php_ini($updated_settings);
}

// Спижжено со стэковерфлоу!
function write_php_ini($array)
{
    global $config_path;

    $res = array();
    foreach ($array as $key => $val) {
        if (is_array($val)) {
            $res[] = "[$key]";
            foreach ($val as $skey => $sval) {
                $res[] = "$skey = " . (is_numeric($sval) ? $sval : '"' . $sval . '"');
            }

        } else {
            $res[] = "$key = " . (is_numeric($val) ? $val : '"' . $val . '"');
        }

    }
    safefilerewrite($config_path, implode("\r\n", $res));
}
// И это тоже
// Надо было побыстрее))
function safefilerewrite($fileName, $dataToSave)
{
    if ($fp = fopen($fileName, 'w')) {
        $startTime = microtime(true);
        do {
            $canWrite = flock($fp, LOCK_EX);
            // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
            if (!$canWrite) {
                usleep(round(rand(0, 100) * 1000));
            }

        } while ((!$canWrite) and ((microtime(true) - $startTime) < 5));

        //file was locked so now we can store information
        if ($canWrite) {fwrite($fp, $dataToSave);
            flock($fp, LOCK_UN);
        }
        fclose($fp);
    }

}

function findSubArrayByKey($array, $key, $value)
{
    foreach ($array as $sub_array) {
        if ($sub_array[$key] === $value) {
            return $sub_array;
        }
    }

    return null;
}


function saveParam($name, $value)
{
    // $_SESSION['campaign'] = $_POST['campaign'];
    // $_SESSION['ad_format'] = $_POST['ad_format'];
    // $_SESSION['cost_type'] = $_POST['cost_type'];
    // $_SESSION['cost_per_type'] = $_POST['cost_per_type'];
    // $_SESSION['category'] = $_POST['category'];
    // $_SESSION['auds'] = $_POST['auds'];
    $_SESSION[$name] = $value;
}

function readParam($name)
{
    return $_SESSION[$name];
}