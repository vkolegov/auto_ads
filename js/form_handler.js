$("#submit_form_button").on('click', function () {
    console.log("processing form");

    $(this).html('<span class="spinner-border spinner-border-sm" style="width: 1.5rem; height: 1.5rem;" role="status" aria-hidden="true"></span> Обработка...');
    $(this).prop('disabled', true);

    clearTextButtonsStyle();
    $(document).trigger("clear-alerts");

    if (validateMessages() == false) {
        console.log("messages not valid");
        addErrorMessage("Не указаны ссылки в сообщениях! Обязательно для CPC");
        resetButton();
        return;
    }

    var group_id = $("#group_select").val();
    var posts_data = gatherPostsData();

    if (!Array.isArray(posts_data) || !posts_data.length) {
        console.error("no posts data");
        addErrorMessage("Не указаны сообщения и вложения");
        resetButton();
        return;
    }

    var ads_settings = gatherAdsSettings();

    var data = {
        'group_id': group_id,
        'posts': posts_data,
        'ads_settings': ads_settings,
    };

    addInfoMessage("Данные обрабатываются");
    sendFormData(data);
});

function gatherPostsData() {
    console.log("gathering posts data");
    var text_buttons = $('#texts_list button').toArray(); // Массив кнопок с текстами
    // var attachments_buttons = $('#attachments_list button').toArray(); // Массив кнопок с текстами

    var posts = [];

    text_buttons.forEach(text_button => {
        var text = $(text_button).data('text');

        // Чистим от лишних пробелов
        text = $.trim(text);

        var text_attachments_id = $(text_button).data('attachments_id');

        // Если все по нулям
        if ((!text || !text.length) &&
            (!text_attachments_id || !text_attachments_id.length)) {
            console.error("Не указан ни текст ни вложения");
            return;
        }

        var post = {};

        if (text && text.length) {
            post['message'] = text;
        }

        if (!text_attachments_id || !text_attachments_id.length) {
            if (post['message'] && post['message'].length) {
                posts.push(post);
            }
            return;
        }

        text_attachments_id_array = JSON.parse(text_attachments_id);

        if (!Array.isArray(text_attachments_id_array) || !text_attachments_id_array.length) {
            console.log("Нет вложений");
            if (post['message'] && post['message'].length) {
                posts.push(post);
            }
            return;
        }

        // Формируем селектор кнопок вложений
        var selector = "";
        text_attachments_id_array.forEach(text_attachment_id => {
            selector = selector + "[data-id=" + text_attachment_id + "], ";
        });
        selector = selector.slice(0, -2); // Отрезаем последние ", "

        // Забираем кнопки которые попадают по data-id
        attachment_buttons = $(selector).toArray();

        // Собираем вложения
        var text_attachments = [];
        attachment_buttons.forEach(attachments_button => {
            text_attachments.push($(attachments_button).data('attachment'));
        });

        post["attachments"] = text_attachments;

        // Группировать вложения?
        var group_attachments = $(text_button).data('group_attachments');

        if (group_attachments != null) {
            post["group_attachments"] = group_attachments;
        }

        posts.push(post);

    });

    console.log("Информация по постах: ");
    console.log(posts);
    return posts;
}

function gatherAdsSettings() {
    console.log("gathering ads settings");
    var campaign_id = $("#campaign_select").val();
    var category_id = $("#category_select").val();
    var ad_format = $("#ad_format_select").val();
    var cost_type = $("#cost_type_select").val();
    var cost_per_type = $("#cost_per_type_text").val();
    var target_groups_selected_options = $("#auds_select option:selected").toArray();
    var sex = $("#sex_select").val();
    var age_from = $("#age_from").val();
    var age_to = $("#age_to").val();
    var country = $("#country_select").val();
    var cities = $("#cities_select").val();
    var cities_not = $("#cities_not_select").val();
    var groups = $("#groups").val();
    var current_group_id = $("#selected-group-id").text();
    var groups_not = current_group_id + $("#groups_not").val();
    var groups_active = $("#groups_active").val();
    var interest_categories = $("#interest_categories_select").val();
    var interests = $("#interests").val();
    var travellers = $("input[name=travellers]").is(":checked");
    var publish = $("input[name=publish]").is(":checked");
    var add_utm = $("input[name=add_utm]").is(":checked");
    var real = $("input[name=real]").is(":checked");

    var target_groups_for_saving_DOM_elements = $("select[name='auditory_for_saving']").toArray();
    var events_for_saving_DOM_elements = $("select[name='auditory_saving_events']").toArray();

    var events_retargeting_groups = gatherEventsRetargetingGroupsData(target_groups_for_saving_DOM_elements, events_for_saving_DOM_elements);

    console.log(events_retargeting_groups);

    var target_groups = gatherTargetGroupsData(target_groups_selected_options);

    var target_settings = {};

    if (sex) {
        target_settings.sex = sex;
    }
    if (age_from) {
        target_settings.age_from = age_from;
    }
    if (age_to) {
        target_settings.age_to = age_to;
    }
    if (country) {
        target_settings.country = country;
    }
    if (Array.isArray(cities) && cities.length) {
        target_settings.cities = cities;
    }
    if (Array.isArray(cities_not) && cities_not.length) {
        target_settings.cities_not = cities_not;
    }
    if (groups) {
        target_settings.groups = groups;
    }
    if (groups_not) {
        target_settings.groups_not = groups_not;
    }
    if (groups_active) {
        target_settings.groups_active = groups_active;
    }
    if (Object.keys(events_retargeting_groups).length !== 0 && events_retargeting_groups.constructor === Object) {
        target_settings.events_retargeting_groups = events_retargeting_groups;
    }
    if (Array.isArray(interest_categories) && interest_categories.length) {
        target_settings.interest_categories = interest_categories;
    }
    if (interests) {
        target_settings.interests = interests;
    }
    if (travellers) {
        target_settings.travellers = travellers;
    }

    var ads_settings = {
        campaign_id: campaign_id,
        category_id: category_id,
        ad_format: ad_format,
        cost_type: cost_type,
        cost_per_type: cost_per_type,
        target_groups: target_groups,
        target_settings: target_settings,
        publish: publish,
        add_utm: add_utm,
        real: real,
    }

    console.log(ads_settings);

    return ads_settings;
}

function gatherEventsRetargetingGroupsData(target_groups_for_saving_DOM_elements, events_for_saving_DOM_elements) {
    var events_retargeting_groups = {}; // Ассоциативный массив он же объект (пнх JS)

    var target_groups_for_saving_elements_count = target_groups_for_saving_DOM_elements.length;
    var events_for_saving_elements_count = events_for_saving_DOM_elements.length;


    if (!target_groups_for_saving_elements_count) {
        console.Error("target groups for saving are not specified");
        return null;
    }

    if (target_groups_for_saving_elements_count != events_for_saving_elements_count) {
        console.Error("element count not matched");
        return null;
    }

    // Одновременно шагаем по двум массивам
    for (let index = 0; index < target_groups_for_saving_elements_count; index++) {

        // Элемент с ретаргет группой
        const target_groups_for_saving_DOM_element = target_groups_for_saving_DOM_elements[index];
        // Элемент с событиями
        const events_for_saving_DOM_element = events_for_saving_DOM_elements[index];

        target_group_for_saving = $(target_groups_for_saving_DOM_element).val();
        events_for_saving = $(events_for_saving_DOM_element).val();

        if (!target_group_for_saving || target_group_for_saving.length === 0) {
            console.warn("Для этой группы не указан ID");
            continue
        }
        if (!Array.isArray(events_for_saving) || !events_for_saving.length) {
            console.warn("Для этой группы не указаны события");
            continue;
        }

        events_retargeting_groups[target_group_for_saving] = events_for_saving;

    }

    return events_retargeting_groups;
}

function gatherTargetGroupsData(target_groups_selected_options) {
    var target_groups = [];

    target_groups_selected_options.forEach(t_g => {
        var target_group_id = $(t_g).val();
        var target_group_name = $(t_g).text();

        var target_group = {
            id: target_group_id,
            name: target_group_name,
        };

        target_groups.push(target_group);
    });

    return target_groups;
}


//
//  Отправка данных
//

function sendFormData(data) {

    var timeout = data['posts'].length * 7500; // 7.5 сек на каждый пост

    console.log("Timeout: " + timeout);

    var request = $.ajax({
        url: "form_handler.php",
        method: "POST",
        data: {
            group_id: data['group_id'],
            posts: JSON.stringify(data['posts']),
            ads_settings: JSON.stringify(data['ads_settings']),
        },
        dataType: "json",
        timeout: timeout,
    });
    request.fail(function (jqXHR, textStatus) {
        console.log(jqXHR);

        resetButton();

        if (jqXHR.status == 500) {
            addErrorMessage("Ошибка 500 при запросе к внутреннему скрипту");
        }
        if (textStatus === 'timeout') {
            addErrorMessage("Время запроса вышло");
        }
    });
    request.done(function (response) {
        console.log("request done");
        resetButton();
        console.log(response);
        outputResponse(response);
    })

}

//
// Сообщения 
//

function outputResponse(response) {

    addMessage(response.variations_status, response.variations_message);

    if (response.ads_posts_status == "success") {

        response.ads_posts_info.forEach(ad_post_info => {
            addMessage(ad_post_info.status, ad_post_info.message);
        });

    } else {
        addMessage(response.ads_posts_status, response.ads_post_message);
    }

    if (response.ads_creation_info) {
        response.ads_creation_info.forEach(ad_creation_info => {
            addMessage(ad_creation_info.status, ad_creation_info.message);
        });
    } else {
        console.error("Нет информации о создании объявлений");
    }
}

function addMessage(priority, message) {
    $(document).trigger("add-alerts", [{
        "message": message,
        "priority": priority
    }]);
}

function addErrorMessage(message) {
    $(document).trigger("add-alerts", [{
        "message": message,
        "priority": 'error'
    }]);
}

function addWarningMessage(message) {
    $(document).trigger("add-alerts", [{
        "message": message,
        "priority": 'warning'
    }]);
}

function addSuccessMessage(message) {
    $(document).trigger("add-alerts", [{
        "message": message,
        "priority": 'success'
    }]);
}

function addInfoMessage(message) {
    $(document).trigger("add-alerts", [{
        "message": message,
        "priority": 'info'
    }]);
}

function resetButton() {
    $("#submit_form_button").html("Создать");
    $("#submit_form_button").prop("disabled", false);
}