$( document ).ready(function() {

    console.log("ready!");

    var request = {
        name: 'countries',
    }

    requestOptions(request, updateSelect, "#country_select");

    request.name = 'cities';
    request.country_id = 1;

    requestOptions(request, updateSelect, "#cities_select, #cities_not_select");

    request.name = 'regions';

    requestOptions(request, updateSelect, "#region_select");

    // Запрашиваем аудитории ретаргетинга
    setTimeout(() => {
        var client_id = $('#account_select').val();

        request.name = 'ads_target_groups';
        request.client_id = client_id;

        requestOptions(request, updateSelect, "#auds_select, [name='auditory_for_saving']");
    }, 1000);

    // Запрашиваем категории интересов
    setTimeout(() => {
        request.name = 'interest_categories';
        requestOptions(request, updateSelect, "#interest_categories_select");
    }, 1500);
});

function updateSelect(selector, options_html){
        var jq_object = $(selector);
        jq_object.html(options_html);
        jq_object.selectpicker('refresh');
        // jq_object.closest('.form-group').find("span.spinner-border").hide();
}


function updateAdsCampaigns(ads_campaigns_options_html) {
    $("#campaign_select").html(ads_campaigns_options_html);
    $("#campaign_select").selectpicker('refresh');
}


function requestOptions(request, callback, selector = null)
{

    $(selector).closest('.form-group').find("span.spinner-border").show();
    $(selector).prop('disabled', true);

    $.post("ajax_handler.php", {
        request: JSON.stringify(request),
    })
    .done(function (data) {
        console.log("request done");
        
        if (selector) {
            $(selector).closest('.form-group').find("span.spinner-border").hide();
            $(selector).prop('disabled', false);
            callback(selector, data);
        } else {
            callback(data);
        }
    });

}