// Сохранение текста
$('button#save_message').on('click', function () {
    var linked_text_id = $(this).data("linkedText");

    // Узнаем id связанной text area
    var text = $("#" + linked_text_id).val();

    // Удаляем лишние пробелы
    text = $.trim(text);

    // Узнаем текущую кнопку
    var active_text_button = $('#texts_list button.active');

    if (text.length == 0) {
        active_text_button.data("text", null);
    } else {
        // Сохраняем текст в КНОПКУ))) 
        active_text_button.data("text", text);
    }

    var new_title = buttonTitleFromText(text);
    // active_text_button.text(new_title);
    var button_title = active_text_button.children('span.title')[0];
    $(button_title).text(new_title);
});

// Реакция на нажатие кнопки на
$('#texts_list button').on('click', function () {

    // Если была нажата неактивная кнопка
    if (!$(this).hasClass('active')) {
        // Выключаем активную
        var active_button = $(this).parent().children("button.active");
        active_button.removeClass('active');

        // Переключаем стиль бэджа на активной кнопке
        var badge_element = active_button.children('.badge')[0];
        $(badge_element).removeClass('badge-light');
        $(badge_element).addClass('badge-primary');
        

        // Считываем текст поста активной кнопки на данный момент
        active_button_text = active_button.data("text");

        // Если текста нет - пишем в заголовке "ПУСТО"
        if (!active_button_text) {
            var active_button_title = active_button.children('span.title')[0];
            $(active_button_title).text("ПУСТО");
        }

        // Делаем активной нажатую
        $(this).addClass('active');

        // Переключаем стиль бэджа нажатой кнопки
        var badge_element = $(this).children('.badge')[0];
        $(badge_element).removeClass('badge-primary');
        $(badge_element).addClass('badge-light');

        // Считываем связанный текст
        var text = $(this).data("text");

        // Если связанный текст задан - записываем его в поле для ввода
        if (text && text.length) {
            $("#message").val(text);
        } else {
            // Иначе пустое поле
            $("#message").val("");

            // А в заголовке - "..."
            var current_button_title = $(this).children('span.title')[0];
            $(current_button_title).text("...");
        }

        // Считываем параметр "группировать вложения"
        var grouped_switch_state = $(this).data('group_attachments');

        if (grouped_switch_state == null) {
            grouped_switch_state = false;
        }
        // Выставляем
        $("#group_attachments_switch").prop('checked', grouped_switch_state);
    }

    // Считываем связанные с этим текстом вложения
    var attachments_id = $(this).data('attachments_id');

    // Если они заданы
    if (attachments_id) {
        var attachments_buttons = $('#attachments_list button').toArray();

        // То включаем те (делаем active) вложения, которые связаны с данным текстом
        attachments_buttons.forEach(attachment_button => {
            // По-любому выключаем
            $(attachment_button).removeClass('active');
            // Считываем id кнопки вложения
            var id = $(attachment_button).data('id');
            var attachment = $(attachment_button).data('attachment');
            // Если id в массиве и вложение существует
            if (attachments_id.includes(id) && attachment) {
                $(attachment_button).addClass('active');
            }
        });
    } else {
        // Если не заданы - ВЫключаем все существующие вложения
        var attachments_buttons = $('#attachments_list button').toArray();

        attachments_buttons.forEach(attachment_button => {
            // По-любому выключаем
            $(attachment_button).removeClass('active');
            // // И сразу включаем если есть связанное вложение
            // if ($(attachment_button).data('attachment')) {
            //     $(attachment_button).addClass('active');
            // }
        });
    }
});

// Реакция на нажатие кнопки в списке вложений
$('#attachments_list button').on('click', function () {
    // Если была нажата неактивная кнопка
    if (!$(this).hasClass('active')) {

        var attachment = $(this).data("attachment");

        // Если нет связанного с кнопкой вложения 
        if (!attachment) {
            // Просим ввести
            attachment = userInput("Введите id вложения", "photo-1_1");

            if (attachment !== null) {
                $(this).data("attachment", attachment);
                var new_title = buttonTitleFromText(attachment)
                $(this).text(new_title);
                // Делаем активной нажатую
                if (attachment != "") {
                    $(this).addClass('active');
                }
            }
        } else {
            // Делаем активной нажатую
            $(this).addClass('active');
        }
    } else {
        // Если была нажата АКТИВНАЯ
        // То делаем ее неактивной
        $(this).removeClass('active');
    }

    updateActiveTextAttachments();
});

// При смене группы
$("#group_select").on('change', function() {
    var new_group_id = $(this).val();

    // Вписываем новый айдишник в список игнорируемых групп
    $("#selected-group-id").text(new_group_id + ",");
});


$("form[name='ad_params']").on('click', "button[name='add_auditory_for_saving']", function(){
    var client_id = $('#account_select').val();
    
    var request = {
        name: "ads_target_groups",
        with_select: true,
        client_id: client_id
    }

    requestOptions(request, createEventsRetargetingGroupsLastForm);

    $(this).hide();   

});

$("form[name='ad_params']").on('click', "button[name='delete_auditory_for_saving']", function(){

    var parent = $(this).closest('.form-row');

    if (countEventsRetargetingGroupsLastForm() != 1) {
        parent.remove();
    } else {
        return;
    }
    var closest_ergf = getEventsRetargetingGroupsLastForm();
    closest_ergf.find("button[name='add_auditory_for_saving']").show();
});

// Редактирование вложения если вызвано контекстное меню
$('#attachments_list button').on('contextmenu', function () {
    var attachment = $(this).data("attachment");

    // Просим ввести вложение
    attachment = userInput("Введите id вложения", attachment);

    if (attachment !== null) {
        $(this).data("attachment", attachment);
        var new_title = buttonTitleFromText(attachment)
        $(this).text(new_title);
        if (attachment == "") {
            $(this).removeClass('active');
        }
    }

    updateActiveTextAttachments();

    return false;
});

// Включаем группировку вложений
$('#group_attachments_switch').on('change', function() {
    var state = $(this).is(":checked");

    // Текущую активную кнопка текста
    var active_text_button = $('#texts_list button.active');

    $(active_text_button).data('group_attachments', state);

    updateActiveTextAttachments();
});

// Если меняем клиента агенства
$('#account_select').on('change', function() {
    console.log("account changed");

    var client_id = $(this).val();

    var request = {
        name: 'ads_campaigns',
        client_id: client_id,
    };

    requestOptions(request, updateAdsCampaigns);

    request.name = 'ads_target_groups';

    requestOptions(request, updateSelect, "#auds_select, [name='auditory_for_saving']");
});

// Если меняем тип оплаты
$('#cost_type_select').on('change', clearTextButtonsStyle);

// При смене региона грузим города
$('#region_select').on('change', function() {
    var country_id = $('#country_select').val();
    var region_id = $(this).val();
    var all = $("#all_cities_switch").is(":checked");

    var request = {
        name: 'cities',
        country_id: country_id,
        region_id: region_id,
        all: all,
    };

    requestOptions(request, updateSelect, "#cities_select, #cities_not_select");
});

$("#all_countries_switch").on('change', function() {

    var all = $(this).is(":checked");

    var request = {
        name: 'countries',
        all: all
    };

    requestOptions(request, updateSelect, "#country_select");
});

$("#all_cities_switch").on('change', function() {
    $('#region_select').trigger('change');
});

// При смене страны грузим регионы
$('#country_select').on('change', function() {
    var country_id = $(this).val();

    if (!country_id) {
        return;
    }

    var all = $("#all_cities_switch").is(":checked");

    var request = {
        name: 'regions',
        country_id: country_id,
    };

    requestOptions(request, updateSelect, "#region_select");

    request.name = 'cities';
    request.all = all;

    requestOptions(request, updateSelect, "#cities_select, #cities_not_select");
});


function createEventsRetargetingGroupsLastForm(formHTML) {

    var closest_ergf = getEventsRetargetingGroupsLastForm();
    closest_ergf.after( formHTML );

    $("select[name='auditory_for_saving']").selectpicker('refresh');   
    $("select[name='auditory_saving_events']").selectpicker('refresh');  
}


function getEventsRetargetingGroupsLastForm()
{
    var closest_ergf = $("select[name='auditory_for_saving']").last().closest(".form-row");
    return closest_ergf;
}

function countEventsRetargetingGroupsLastForm() {
    var ergfs = $("select[name='auditory_for_saving']").toArray();

    return ergfs.length;
}

//
// Helper functions
//

function userInput(title, text) {
    var input = prompt(title, text);

    if (input == null) {
        return null;
    } else {
        return $.trim(input);
    }
}

function buttonTitleFromText(text) {
    const max_length = 25;
    const empty_title = "ПУСТО";
    // Если текст длиннее чем максимальное количество
    if (text.length > max_length) {
        // Часто оставляем, остальное заменяем троеточиями
        var shortened_text = text.substr(0, max_length - 3) + "...";
        return shortened_text;
    } else if (text.length == 0) {
        // Если по нулям - заголовок будет "ПУСТО"
        return empty_title;
    } else {
        // А если текст не длинный - используем целиком
        return text;
    }
}

function clearTextButtonsStyle() {
    var text_buttons = $('#texts_list button').toArray();
        
    // Чиcтим внешний вид
    text_buttons.forEach(text_button => {

        $(text_button).removeClass('list-group-item-danger');
        $(text_button).removeClass('list-group-item-success');

    });
}

function updateActiveTextAttachments()
{
    // Собираем id "включенных" вложений
    var active_buttons = $('#attachments_list button.active').toArray();
    var active_ids = [];
    active_buttons.forEach(active_button => {
        active_ids.push($(active_button).data('id'));
    });

    // Сгруппированы ли вложения
    var attachments_grouped = $('#group_attachments_switch').is(":checked");

    // Узнаем текущую кнопку
    var active_text_button = $('#texts_list button.active');

    // Считаем количество вложений
    var attachments_count = active_ids.length;

    // Бэдж с количеством вложений
    var badge_element = active_text_button.children('.badge')[0];

    // Обновляем количество вложений в бэдже
    if (!attachments_grouped) {
        $(badge_element).text(attachments_count);
    } else {
        $(badge_element).text('['+attachments_count+']');
    }

    if (attachments_count == 0) {
        active_text_button.data('attachments_id', null);
    } else {
        // Сохраняем выбранные вложения В КНОПКУ)))
        active_text_button.data('attachments_id', JSON.stringify(active_ids));
    }
}