function validateMessages() {
    console.log("validating messages");
    var cost_type = $('#cost_type_select').val();
    

    $validation_passed = true;

    // Если CPC
    if (cost_type == 0) {
        console.log("validating messages CPC");

        var text_buttons = $('#texts_list button').toArray();
        var expr = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi;
        var regexp = new RegExp(expr);
        // Чекаем инпут
        text_buttons.forEach(text_button => {
            
            var text = $(text_button).data('text');

            // Если текст пустой - срать
            if (!text) {
                return;
            }

            var text_has_link = regexp.test(text);
            console.log(text + ": text_has_link");
            regexp.lastIndex = 0; // Скидываем ласт индекс

            $(text_button).removeClass('list-group-item-danger');
            $(text_button).removeClass('list-group-item-success');

            if (!text_has_link) {
                $(text_button).addClass('list-group-item-danger');
                $validation_passed = false;
            } else {
                $(text_button).addClass('list-group-item-success');
            }
        });
    }

    return $validation_passed;
}