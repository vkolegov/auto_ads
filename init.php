<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
header('Content-type: text/html; charset=utf-8');

require_once('globals.php');

require_once BASEPATH . "/classes/class.logger.php";
require_once BASEPATH . "/classes/class.vkapihelper.php";

require_once('basic_functions.php');

$init_logger = new Logger('INIT', 'file', Logger::MODE_ALL);

// Если указаны id приложения и защитный ключ НО НЕ УКАЗАН код (ответ от oauth вк)
if ( isset($_GET['app_id']) &&
    isset($_GET['app_secret']) &&
    !isset($_GET['code']) ) {

    $app_id = $_GET['app_id'];
    $app_secret = $_GET['app_secret'];

    $init_logger->logInfo("[init.php] Указан app_id: " . $app_id);
    $init_logger->logInfo("[init.php] Указан app_secret: " . $app_secret);
    $init_logger->logInfo("[init.php] Вызываем VKApiHelper::buildCodeUrl()");

    $url = VKApiHelper::buildCodeUrl($app_id, $app_secret);

    $init_logger->logInfo("[init.php] Делаем редирект на vk для авторизации и получения кода");
    header('Location: ' . $url);

} else if ( isset($_GET['code']) &&
            isset($_GET['app_id']) && 
            isset($_GET['app_secret']) ) {

    $code = $_GET['code'];
    $app_id = $_GET['app_id'];
    $app_secret = $_GET['app_secret'];

    $init_logger->logInfo("[init.php] Указан code: " . $code);
    $init_logger->logInfo("[init.php] Указан app_id: " . $app_id);
    $init_logger->logInfo("[init.php] Указан app_secret: " . $app_secret);

    $init_logger->logInfo("[init.php] Вызываем VKApiHelper::getToken()");

    $access_token = VKApiHelper::getToken($app_id, $app_secret, $code);

    if ($access_token) {
        $init_logger->logInfo("[init.php] Получен токен " . $access_token);
    } else {
        $init_logger->logInfo("[init.php] Токен не получен");
    }

} else if ( isset($_GET['access_token']) ) {
    $access_token = $_GET['access_token'];

    $init_logger->logInfo("[init.php] Указан access_token: " . $access_token);

    $init_logger->logInfo("[init.php] Вызываем VKApiHelper::saveToken()");

    VKApiHelper::saveToken($access_token);

} else {
    $init_logger->logWarning("[init.php] Не указан ни один интересный GET параметр");
}

if ( !empty($_GET['account_id']) ) {
    $account_id = $_GET['account_id'];
    if (isset($_GET['agency'])) {
      $agency = true;
    } else {
      $agency = false;
    }

    $init_logger->logInfo("[init.php] Указан account_id: " . $account_id);

    $init_logger->logInfo("[init.php] Вызываем VKApiHelper::saveAdsAccountId()");

    VKApiHelper::saveAdsAccountId($account_id, $agency);
}

if ( !empty($_GET['group_id']) ) {
  $group_id = $_GET['group_id'];

  $init_logger->logInfo("[init.php] Указан group_id: " . $group_id);

  $init_logger->logInfo("[init.php] Вызываем VKApiHelper::updateGroupId()");

  VKApiHelper::updateGroupId($group_id);
}

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"> -->
    <!-- /CSS -->
    <title> Первоначальная настройка динамической обложки </title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Объявления VK</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="./">Создание объявлений</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="./init.php">Настройка  <span class="sr-only">(текущая)</span></a>
      </li>
    </ul>
  </div>
</nav>
<div class="container">
<h1> Настройка основных параметров</h1> <br>
<?php include 'forms/get_token_form.html'; ?>
<br>-------------------<br>
<?php include 'forms/save_token_form.html'; ?>
<br>-------------------<br>
<?php include 'forms/save_ads_account.html'; ?>
</div>
<!-- JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-ru_RU.min.js"></script> -->
<script>
    $(function () {
        $('select').selectpicker();
    });
</script>
<!-- /JS -->
</body>
</html>

<?php

?>