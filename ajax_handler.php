<?php

require_once 'globals.php';
require_once 'classes/class.logger.php';
require_once 'classes/class.vkapihelper.php';
require_once 'html_generator.php';

$logger = new Logger('HTMLGENERATOR', 'file', Logger::MODE_ALL);

$vkh = new VKApiHelper();


if (isset($_POST['request'])) {
    $logger->logInfo("Есть запрос " . $_POST['request']);
    $request = json_decode($_POST['request'], true);
    $request_name = $request['name'];

    switch ($request_name) {
        case 'countries':

            if (isset($request['all'])) {
                $need_all = $request['all'] ? 1 : 0;
            } else {
                $need_all = 0;
            }

            $countries = $vkh->getCountries($need_all);
            
            echo generateOptionsHTML($countries, 'id', 'title');
            break;
        case 'regions':
            $country_id = $request['country_id'];
            $regions = $vkh->getRegions($country_id);

            
            echo "<option disabled selected value>Выберите регион</option>" . generateOptionsHTML($regions);
            
            break;
        case 'cities':
            $country_id = $request['country_id'];
            $region_id = $request['region_id'];
            if (isset($request['all'])) {
                $need_all = $request['all'] ? 1 : 0;
            } else {
                $need_all = 0;
            }
            $cities = $vkh->getCities($country_id, $region_id, $need_all);

            echo generateOptionsHTML($cities);
            
            break;
        
        case 'ads_campaigns':
            $client_id = $request['client_id'];
            $ad_campaigns = $vkh->getAdsCampaigns($client_id);

            echo generateOptionsHTML($ad_campaigns, 'id', 'name');

            break;
        case 'ads_target_groups':
            $client_id = $request['client_id'];
            $ads_target_groups = $vkh->getAdsTargetGroups($client_id);

            $target_groups_options_HTML = generateOptionsHTML($ads_target_groups, 'id', 'name', 'audience_count');
            $events_options_HTML = generateOptionsHTML($events, 'key', 'element');


            if (isset($request['with_select'])) {
                echo generateSelectHTML("forms/save_retargeting_groups_template.html", array($target_groups_options_HTML, $events_options_HTML));

            } else {
                echo $target_groups_options_HTML;
            }

            break;
        case 'interest_categories':
            $interest_categories = $vkh->getAdsSuggestions('interest_categories_v2');
            echo generateOptionsHTML($interest_categories, 'id', 'name');

            break;
        default:
            $logger->logError("Неизвестный реквест " . $request_name);
            break;
    }

}

//
// Генерирование ответа
//


function generateJSONResponse($response)
{

    header('Content-Type: application/json;;charset=utf-8');
    echo json_encode($response);

}

?>