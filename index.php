<?php

require_once 'globals.php';
require_once 'classes/class.vkapihelper.php';
require_once 'classes/class.logger.php';
require_once 'html_generator.php';

$logger = new Logger('INDEX', 'file', Logger::MODE_ALL);
$vkh = new VKApiHelper();

$logger->logInfo("Старт");

$campaign_id = readParam('campaign_id');
$ad_format = readParam('ad_format');
$category_id = readParam('category_id');
$cost_type = readParam('cost_type');
$cost_per_type = readParam('cost_per_type');
$target_groups = readParam('target_groups');
if (!empty($target_groups)) {
	$auds = array_column($target_groups, 'id');
}

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <!-- /CSS -->
    <title> Объявления VK </title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="./">Объявления VK</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Создание объявлений <span class="sr-only">(текущая)</span></a>
      </li>
    </ul>
  </div>
</nav>
<div class="container">
  <h1> Создание рекламных объявлений</h1> <br>
  <?php include 'ad_params.html';?>
</div>
<!-- JS -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-ru_RU.min.js"></script>
<script src="js/bs-alerts.min.js"></script>
<script src="js/validation.js"></script>
<script src="js/async_load.js"></script>
<script src="js/input_handlers.js"></script>
<script src="js/form_handler.js"></script>
<script>
    $(function () {
        $('select').selectpicker({dropupAuto:false, virtualScroll: 50});
    });
</script>
<!-- /JS -->
</body>
</html>

<?php
$logger->logInfo("Финиш");

?>