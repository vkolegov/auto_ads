<?
require_once 'globals.php';
require_once 'classes/class.logger.php';
require_once 'classes/class.vkapihelper.php';
require_once 'utm.php';

$logger = new Logger("HANDLER", 'file', Logger::MODE_WARN);

$logger->logInfoI("Старт");

if (isset($_POST['group_id'], $_POST['posts'], $_POST['ads_settings'])) {
    $group_id = $_POST['group_id'];
    $posts = json_decode($_POST['posts'], true);
    $ads_settings = json_decode($_POST['ads_settings'], true);
    $logger->logInfoI("ID группы: {1}", $group_id);
    $logger->logInfoI("Информация о постах: {1}", $posts);
    $logger->logInfo("Настройки объявлений: {1}", $ads_settings);

    // Сохраняем настройки объявлений в сессии
    foreach ($ads_settings as $key => $value) {
        saveParam($key, $value);
    }

    // Валидация
    if (!validateData($posts, $ads_settings)) {
        $logger->logError("Данные не прошли валидацию");
        return;
    }
    // Если все в порядке - продолжаем
    // TODO: Сохранять в сессию!

    $real = false;
    if (isset($ads_settings['real'])) {
        $logger->logWarning("Выполняем реальные действия");
        $real = $ads_settings['real'];
    }

    if (isset($ads_settings['add_utm'])) {
        $add_utm = $ads_settings['add_utm'];
    }

    if ($ads_settings['target_groups']) {
        $target_groups = $ads_settings['target_groups'];
    }

    $post_aud_variations = createVariations($posts, $target_groups);

    $data = array();

    if (!isset($post_aud_variations)) {
        $data['variations_status'] = 'error';
        $data['variations_message'] = "Не удалось создать комбинации сообщений";
        generateResponse($data);
        return;
    } else {
        $data['variations_status'] = 'info';
        $data['variations_message'] = "Создано " . count($post_aud_variations) . " вариаций";
    }

    $logger->logInfo("Вариации: {1}", $post_aud_variations);

    $ads_posts_info = createStealthAdsPosts($group_id, $post_aud_variations, $real);

    if (!isset($ads_posts_info)) {
        $data['ads_posts_status'] = 'error';
        $data['ads_posts_message'] = "Не удалось создать посты";
        generateResponse($data);
        return;
    } else {
        $data['ads_posts_status'] = 'success';
        $data['ads_posts_info'] = array();

        foreach ($ads_posts_info as $ads_post) {

            if ($ads_post['error']) {
                $status = "error";
                $message = "Пост для будущего объявления \"" .$ads_post['name'] . "\" не был создан. Ошибка " . $ads_post['error'];
                $logger->logError($message);
                // array_push($data['ads_posts'], $error_msg);
            } else {
                $status = "success";
                $logger->logInfoI("Создан пост. ссылка " . $ads_post['url']);
                $message = "Создан пост. Ссылка <a href=\"" . $ads_post['url'] . "\" target=\"_blank\" > " . $ads_post['url'] . "</a>";
            }

            $ad_post_info = ['status' => $status, 'message' => $message];

            array_push($data['ads_posts_info'], $ad_post_info);
        }
    }

    $logger->logInfo("Информация о постах: {1}", $ads_posts_info);

    $ads_data = createAdsSpecifications($ads_settings, $ads_posts_info);

    $logger->logInfoI("Спецификация объявлений: {1}", $ads_data);

    $ads_creation_info = createAds($ads_data, $real);

    if (isset($ads_creation_info['error'])) {
        $data['ads_creation_status'] = 'error';
        $data['ads_creation_message'] = $ads_creation_info['error_msg'];
        generateResponse($data);
        return;
    } elseif (is_string($ads_creation_info)) {
        $data['ads_creation_status'] = 'info';
        $data['ads_creation_message'] = $ads_creation_info;
    } else {

        $data['ads_creation_info'] = array();

        $ads_count = count($ads_data);

        for ($i=0; $i < $ads_count; $i++) {

            $ad_creation_info = array();

            $message = "Объявление с именем " . $ads_data[$i]['name'];

            if ($ads_creation_info[$i]['id'] == 0) {
                $status = "error";
                $message = $message . " НЕ БЫЛО создано. Причина: " . $ads_creation_info[$i]['error_desc'];
            } else {

                if ($add_utm) {
                    $logger->logInfo("Добавляем UTM метки");
                    addUtmToLinksInPosts($group_id, $post_aud_variations[$i], $ads_posts_info[$i], $ads_data[$i], $ads_creation_info[$i], $real);
                } else {
                    $logger->logInfo("Не добавляем UTM метки");
                }

                if ( isset($ads_creation_info[$i]['error_desc']) ) {
                    $status = "warning";
                    $message = $message . " <a href=\"https://vk.com/ads?act=office&union_id=" . $ads_creation_info[$i]['id'] . "\" target=\"_blank\" >было создано</a> с ошибками. Описание ошибок: " . $ads_creation_info[$i]['error_desc'];
                } else {
                    $status = "success";
                    $message = $message . " <a href=\"https://vk.com/ads?act=office&union_id=" . $ads_creation_info[$i]['id'] . "\" target=\"_blank\" >успешно создано.</a> Его ID " .$ads_creation_info[$i]['id'];
                }


            }

            $ad_creation_info = ['status' => $status, 'message' => $message];

            array_push($data['ads_creation_info'], $ad_creation_info);
        }
    }

    generateResponse($data);

} else {
    $logger->logError("Информация не полная");
    return;
}

$logger->logInfoI("Финиш");
//======================================================
//=======================ФУНКЦИИ========================
//======================================================

//
// Валидация
//
function validateData($posts, $ads_settings)
{
    global $logger;

    if (!validateAdsSettings($ads_settings)) {
        $logger->logError("[validateData()] Рекламные настройки не в порядке");
        return false;
    } else {
        $logger->logInfo("[validateData()] Рекламные настройки в порядке!");
    }

    $cost_type = $ads_settings['cost_type'];
    $messages = array_column($posts, 'message');

    if (!validateMessages($messages, $cost_type)) {
        $logger->logError("[validateData()] Сообщения не в порядке");
        return false;
    } else {
        $logger->logInfo("[validateData()] Сообщения в порядке!");
    }

    return true;
}

function validateMessages($messages, $cost_type)
{
    global $logger;

    foreach ($messages as $message) {


        // Проверяем наличие ссылок если указан тип CPC
        if ($cost_type == 0) {

            // Если ссылка в сообщении не обнаружена - сразу возвращаем false и не проверяем остальные
            if (!checkForLinks($message)) {
                $logger->logError("[validateMessages()] В сообщении нет ссылок!");
                return false;
            } else {
                $logger->logInfo("[validateMessages()] В сообщении есть ссылки, все норм!");
            }

        }
    }

    return true;
}

function validateAdsSettings($ads_settings)
{
    global $logger;

    $logger->logInfo("[validateAdsSettings()]");
    if (!isset($ads_settings['campaign_id'],
        $ads_settings['category_id'],
        $ads_settings['ad_format'],
        $ads_settings['cost_type'],
        $ads_settings['cost_per_type']
    )) {
        return false;
    } else {
        return true;
    }
}

//
// Создание вариаций
//

function createVariations($posts, $target_groups)
{
    global $logger;

    $variations = array();

    $post_variations = array();

    foreach ($posts as $post_id => $post) {
        $post_variation = array();

        if (!isset($post['message']) && !isset($post['attachments'])) {
            $logger->logError("[createVariations()] Ни сообщения ни вложений");
            continue;
        }

        if (isset($post['message'])) {
            $logger->logInfo("[createVariations()] Задано сообщение");

            $message = $post['message'];
            $post_variation['message'] = $message;
            $post_variation['message_id'] = $post_id + 1;
        }

        if (isset($post['attachments'])) {
            $logger->logInfo("[createVariations()] Заданы вложения");
            $attachments = $post['attachments'];

            // Если надо сгруппировать - группируем
            if (isset($post['group_attachments'])) {
                $post_variation['attachments'] = $post['attachments'];
                array_push($post_variations, $post_variation);
            } else {
                // Иначе хуярим комбинации
                foreach ($attachments as $attachment_id => $attachment) {
                    $post_variation['attachment'] = $attachment;
                    $post_variation['attachment_id'] = $attachment_id + 1;
                    array_push($post_variations, $post_variation);
                }
            }
        } else {
            $logger->logInfo("[createVariations()] Вложения не заданы");
            array_push($post_variations, $post_variation);
        }
    }

    // Делаем комбинации с аудиториями

    if (!isset($target_groups)) {
        $variations = $post_variations;
    } else {
        foreach ($post_variations as $post_variation) {
            foreach ($target_groups as $target_group) {
                $variation = $post_variation;
                $variation['target_group_id'] = $target_group['id'];
                $variation['target_group_name'] = $target_group['name'];
                array_push($variations, $variation);
            }
        }
    }

    return $variations;
}

function createStealthAdsPosts($group_id, $posts, $real = true)
{
    global $logger;

    $vkh = new VKApiHelper();

    $logger->logInfo("[createStealthAdsPosts()]");

    $posts_info = array();
    $post_id = 0;
    foreach ($posts as $post) {

        if ($real) {
            $attachments_param = null;
            $error = null; // Сбрасываем ошибку
            $post_id = null; // Сбрасываем ID поста

            if (isset($post['attachment'])) {
                $logger->logInfo("[createStealthAdsPosts()] ВЛОЖЕНИ_Е_");
                $attachments_param = $post['attachment'];
            } elseif (isset($post['attachments'])) {
                $logger->logInfo("[createStealthAdsPosts()] ВЛОЖЕНИ_Я_");
                $attachments_param = $post['attachments'];
            } else {
                $logger->logInfo("[createStealthAdsPosts()] НЕТ ВЛОЖЕНИЙ ПИЗДОС");
            }

            $post_creation_response = $vkh->postAdsStealth($group_id, $post['message'], $attachments_param);

            if (is_numeric($post_creation_response)) {
                $post_id = $post_creation_response;
            } else if (is_string($post_creation_response)) {
                $logger->logError("[createStealthAdsPosts()] Ошибка создания поста: {1}", $post_creation_response);
                $error = $post_creation_response;
            }

            
        } else {
            $post_id++;
        }

        // Собираем ссылку

        $ads_post_url = "http://vk.com/wall-" . $group_id . "_" . $post_id;

        $post_info = array(
            'id' => $post_id,
            'url' => $ads_post_url,
            'target_group_id' => $post['target_group_id'],
            'name' => generateAdName($post),
        );
    
        // Если была ошибка - цепляем ее к информации о посте
        if (isset($error)) {
            $post_info['error'] = $error;
        }

        array_push($posts_info, $post_info);

    }

    return $posts_info;
}

function createAdsSpecifications($ads_settings, $ads_posts_info)
{
    global $logger;

    $logger->logInfo("[createAdsSpecifications()]");

    $vkh = new VKApiHelper();

    $campaign_id = $ads_settings['campaign_id'];
    $ad_format = $ads_settings['ad_format'];
    $category_id = $ads_settings['category_id'];
    $cost_type = $ads_settings['cost_type'];
    $cost_per_type = $ads_settings['cost_per_type'];
    $publish = $ads_settings['publish'];

    $target_settings = $ads_settings['target_settings'];

    $ads_data = array();

    foreach ($ads_posts_info as $ads_post_info) {

        if (isset($ads_post_info['error'])) {
            $logger->logError("[createAdsSpecifications()] Пост с ошибкой: {1}", $ads_post_info['error']);
            continue;
        }

        $ad_data = $vkh->createAdSpecification(
            $campaign_id,
            $ad_format,
            $category_id,
            array($ads_post_info['target_group_id']),
            $cost_type,
            $cost_per_type,
            $ads_post_info['url'],
            $ads_post_info['name'],
            $publish,
            $target_settings
        );

        array_push($ads_data, $ad_data);
    }

    return $ads_data;
}

function generateAdName($post)
{
    global $logger;
    // Собираем имя объявления
    $ad_name = "";

    $attachments_types = array(
        'photo' => 'фото',
        'video' => 'видео',
        'audio' => 'аудио',
        'doc' => 'документ',
        'page' => 'wiki-страница',
        'note' => 'заметка',
        'poll' => 'опрос',
        'album' => 'альбом',
        'market' => 'товар',
        'market_album' => 'подборка товаров',
        'audio_playlist' => 'плейлист',
    );

    if (isset($post['attachment'], $post['attachment_id'])) {

        $detected_type = "неизв.св";

        foreach ($attachments_types as $type => $name) {
            $regexpr = $type . "\-?\d+\_\d+";

            mb_ereg_search_init($post['attachment'], $regexpr);
            $r = mb_ereg_search();

            // $logger->logInfo("[generateAdName()] Массив regs после проверки \"" . $post['attachment'] . "\" : " . var_export($r, true));
            if ($r) {
                $logger->logInfo("[generateAdName()] Определён тип: {1}", $name);
                $detected_type = $name;
                break;
            }
        }

        $logger->logInfo("[generateAdName()] Вложение {1} тип {2}", $post['attachment'], $detected_type);

        $ad_name = $detected_type . " " . $post['attachment_id'];
    }

    if (isset($post['attachments'])) {
        $ad_name = "неск. вложений";
    }

    if (isset($post['message'], $post['message_id'])) {
        if ($ad_name !== "") {
            $ad_name = $ad_name . " | ";
        }
        $ad_name = $ad_name . "текст " . $post['message_id'];
    }

    if (isset($post['target_group_id'])) {

        $target_group_name = $post['target_group_name'];

        if ($ad_name !== "") {
            $ad_name = $ad_name . " | ";
        }

        $ad_name = $ad_name . $target_group_name;
    }

    $logger->logInfo("[generateAdName()] Сгенерированное имя объявления: {1}", $ad_name);

    return $ad_name;
}

function createAds($ads_data, $real = true)
{
    global $logger;

    $vkh = new VKApiHelper();

    if ($real) {
        $ads_creation_info = $vkh->createAds($ads_data);
        return $ads_creation_info;
    } else {
        $logger->logInfo("[createAds()] Не выполняем реальных действий");
        return "Тестовый запуск успешен";
    }
}

function generateVKPostUTM($cost_type, $campaign_id, $ad_id) {

    global $logger;
    // Генерируем UTM метку
    switch ($cost_type) {
        case 0:
            $medium = "cpc";
            break;
        case 1:
            $medium = "cpm";
            break;
        default:
            $logger->logError("cost_type не CPC(0) или CPM(1) а " . $cost_type);
            break;
    }

    $utm = generateUtmParams("vk", $medium, $campaign_id, $ad_id);

    return $utm;
}

function checkForLinks($message){
    global $logger;

    $links = extractLinks($message);

    $logger->logInfoI("[checkForLinks()] Ссылки в \"$message\" : " . var_export($links, true));
    if (empty($links)) {
        $logger->logError("[checkForLinks()] В сообщении нет ссылки!", true);
        return false;
    } else {
        $logger->logInfoI("[checkForLinks()] В сообщении есть ссылка, все норм!");
        return true;
    }

    return true;
}

function extractLinks($message, $exclude_regexp = null) {

    global $logger;

    $regs = array(); // Результат поиска
    $url_pattern = '#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si';

    preg_match_all($url_pattern, $message, $regs);

    if (empty($regs[0])) {
        return null;
    }

    $links = $regs[0];

    if (isset($exclude_regexp) && is_string($exclude_regexp)) {
        $logger->logInfoI("[extractLinks()] Указана регулярка для исключений");

        $links_count = count($links);

        for ($i=0; $i < $links_count; $i++) {

            if (mb_ereg_match($exclude_regexp, $links[$i])) {
                $logger->logInfoI("[extractLinks()] Ссылка " . $links[$i] . " подходит под исключение");
                unset($links[$i]);
            }

        }

    } else {
        $logger->logInfoI("[extractLinks()] Не указана или неверно указана регулярка для исключений");
    }

    return $links;
}

function extractGetParams($url_string) {
    $regs = array(); // Результат

    $get_params_pattern = "#\?.+\=.+#";

    preg_match_all($get_params_pattern, $url_string, $regs);

    if (empty($regs[0])) {
        return null;
    }

    return $regs[0];
}

function addUtmToLinksInPosts($group_id, $post_variation, $ad_post_info, $ad_data, $ad_creation_info, $real = true) {
    global $logger;

    $vkh = new VKApiHelper();

    $logger->logInfoI("Вариация поста: " . var_export($post_variation, true));
    $logger->logInfoI("Информация о посте: " . var_export($ad_post_info, true));
    $logger->logInfoI("Спецификация: " . var_export($ad_data, true));
    $logger->logInfoI("Информация об объявлении: " . var_export($ad_creation_info, true));

    $utm = generateVKPostUTM($ad_data['cost_type'], $ad_data['campaign_id'], $ad_creation_info['id']);

    if ($real) {
        $message = $post_variation['message'];// . " UTM: " . $utm;

        $exclude_regexp = "^(https?:\/\/)?vk\.(com|cc)\/?.*$";

        $links = extractLinks($message, $exclude_regexp);

        $logger->logInfoI("[addUtmToLinksInPosts()] Ссылки: " . var_export($links, true));

        if (!isset($links) || empty($links)) {
            $logger->logWarning("[addUtmToLinksInPosts()] Ссылок нет");
            return;
        }

        foreach ($links as $link) {
            $get_params = extractGetParams($link);

            $logger->logInfoI("[addUtmToLinksInPosts()] Гет параметры: " . var_export($get_params, true));

            // Учитываем что возможно уже есть GET-параметры в ссылке
            if ($get_params) {
                $glue = "&";
            } else {
                $glue = "?";
            }

            $utm_link = $link . $glue . $utm; // http://ya.ru/ => http://ya.ru/?utm_source=vk&utm...

            $message = mb_ereg_replace(preg_quote($link), $utm_link, $message);

        }

        $logger->logInfoI("[addUtmToLinksInPosts()] Сообщение с обновленными ссылками: " . $message);

        if (isset($post_variation['attachment'])) {
            $logger->logInfoI("[addUtmToLinksInPosts()] ВЛОЖЕНИ_Е_");
            $vkh->editPostAdsStealth($group_id, $ad_post_info['id'], $message, $post_variation['attachment']);

        } elseif (isset($post_variation['attachments'])) {
            $logger->logInfoI("[addUtmToLinksInPosts()] ВЛОЖЕНИ_Я_");
            $vkh->editPostAdsStealth($group_id, $ad_post_info['id'], $message, $post_variation['attachments']);
        } else {
            $logger->logInfoI("[addUtmToLinksInPosts()] НЕТ ВЛОЖЕНИЙ ПИЗДОС");
            $vkh->editPostAdsStealth($group_id, $ad_post_info['id'], $message, null);
        }
    }
}

//
// Генерирование ответа
//

function generateResponse($data)
{
    header('Content-Type: application/json;;charset=utf-8');
    echo json_encode($data);

}
